# EMG Signal Analysis for Classification of Hyperkinetic Movement Disorders
Hyperkinetic movement disorders are most of the time easy to spot, but they are hard to differentiate between the different
movement disorders. This makes it very hard to diagnose the right movement disorder. To fix this problem the "Next Move
in Movement Disorders" (NEMO) research group by creating an artificial intelligence to can diagnose patients by using 3d video,
Inertial Measurement Unit (IMU), Magnetic Resonance Imaging (MRI), Positron Emission Tomography (PET) and Electromyography (EMG)
data. A lot of research has already gone into the 3d video, IMU, MRI and PET data, yet not a lot of research has gone into
the EMG data. So the focus in this research is on the EMG data and will use Tremors and Myoclonuses as movement disorders

## Getting Started
### Installation
Clone the repository from the Bitbucket page and change the values in the file "__config.yaml__" to the wanted parameters.
The cloning of the repository can be done by:

```bash
git clone https://RinzePieterJonker@bitbucket.org/RinzePieterJonker/hyperkinetic-movement-disorder-classifier-intership-nemo-umcg.git
```

### Prerequisites
#### Packages
The packages that where used for this research can also be retrieved from the python anaconda package (https://www.anaconda.com/distribution/)

|Package     | Version   | Installation                                 |
|:----------:|:---------:|----------------------------------------------|
|sys         |           |                                              |
|warnings    |           |                                              |
|pywt        | 0.5.2.    | ```pip install Pywavelets==0.5.2```          |
|csv         | 1.0.      |                                              |
|argparse    | 1.1.      | ```pip install argparse```                   |
|yaml        | 3.12.     |                                              |
|os          |           |                                              |
|time        |           |                                              |
|pickle      |           |                                              |
|numpy       | 1.15.2.   | ```python -m pip install --user numpy```     |
|pandas      | 0.23.0.   | ```pip install pandas```                     |
|matplotlib  | 2.2.2.    | ```python -m pip install --user matplotlib```|
|seaborn     | 0.8.1.    | ```pip install seaborn```                    |
|statsmodels | 0.9.0.    | ```pip install statsmodels```                |
|math        |           |                                              |
|scipy       | 1.1.0.    | ```python -m pip install --user scipy```     |
|skLearn     | 0.20.2.   | ```pip install -U scikit-learn```            |
|cv2 (openCV)| 3.4.4.    | ```pip install opencv-python```              |
|gc          |           |                                              |
|glob        |           |                                              |


## Usage
### EmgSignal.py
The script can be executed by using the following bash command:
```bash
EmgSignal.py --exercise <EXERCISE / EXERCISES> --patients <PATIENT / PATIENTS> --modality <MODALITY>
```
or for the help, this will automatically end the script
``` bash
EmgSignal.py -h
```
#### Parameters
 Parameter      | Default   | Function
----------------|:---------:|--------------------------------------------------------------------------------
-e, --exercise  |           | These are all the exercises used for the script, multiple exercises are separated by spaces
-p, --patients  |           | These are all the patients used for the script, multiple patients are separated by spaces
-m, --modality  | IMU_EMG   | This is the modality used for the script, only EMG and IMU_EMG are supported
-h, --help      |           | This sends out the help text of the script

### OneVsOne.py
This script can be used as a classifier of Tremors, Myoclonus and Healthy patients. It will generate 2 meta classifiers,
these are to train and validate with different patient, these have the tag A and B. There are also multiple OneVsOne classifiers
that have the tag Trad, the classifiers use a traditional style of test and training set where patient is not taken in to account,
These classifiers are not used for a meta classifier because the show a lot of over-fitting on the data set . This code can be used by using the 
following bash command:
```bash
OneVsOne.py --patients <PATIENTS> --exercise <EXERCISE / EXERCISES>
```
or for the help text:
```bash
OneVsOne.py -h
```

#### Parameters
 Parameter      | Default   | Function
 ---------------|:---------:|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -p, --patients |           | The patients that are going to be used for the training and validation of the classifier
 -e, --exercise |           | The exercise that is going to be used for the training and validation of the classifier
 -m, --modality | IMU_EMG   | The modality that is going to be used for the training and validation of the classifier, only IMU_EMG and EMG are supported
 -s, --step     | 333       | The step that is taken between the overlapping windows
 -n, --size     | 3000      | The size of the overlapping windows
 -f, --filter   |           | The amounts of Hz used for the bandpass filter (highpass, lowpass)
 -r, --runs     | 1         | The amount of runs that the code is going to do
 -o, --overwrite| False     | If the datafiles that have been created by previous runs will be used for the classifier or if new data files will be created 
 -P, --predict  |           | The patients that you want to predict, this will generate  a classification and a video classification if a video is present
 -S, --sensors  | [4, 6]    | The sensors that are going to be used for the training and validation of the classifier, sensors can be checked in the sensorEMG.csv file and sensorIMU_EMG.csv file depending on the modality
 
## Files included
 - __Scripts/__
    - __EmgSignal.py__ - This script creates all the EmgSignal objects used by the classifiers, this script can also be 
    used to visualize the EMG data for each patient 
    - __OneVsOne.py__ - This script is used to create a meta classifier for one exercise using OneVsOne objects. These 
    OneVsOne objects are classifiers that can classify between 2 disorders and inherits functions from the OneVsAll
    objects
    - __OneVsAll.py__ - This script is used to create a meta classifier for one exercise using OneVsAll object. These
    OneVsAll objects try to classify windows if they are from a disorder of if they aren't from a disorder
    - __extractData.py__ - This function is used to extract the EMG signal form the .npy files. This code was written by
    Joost Calon
 - __Data/__
    - __Model/__ - This is where all the classifiers are saved
    - __Scalers/__ - This is the directory where all the trained MinMaxScalers are saved
 - __Docs/__
    - __Electromyography feature analysis for classifciation of hyperkinetic movement disorders.pdf__ - This is the 
   research paper for this code
 - __Pictures/__ - This is the directory where all the figures are stored
 - __config.yaml__ - The config file used for the code
 - __diagnosis.csv__ - The diagnosis of the patients per ID used by NEMO
 - __sensorEMG.csv__ - The names for the sensors with the EMG modality
 - __sensorIMU_EMG.csv__ - The names for the sensors with the IMU_EMG modality 
 - __ReadMe.md__ - The read me of this project (This file)
 
## Known issues
 - _C:\ProgramData\Anaconda3\lib\site-packages\scipy\signal\_arraytools.py:45: FutureWarning: Using a non-tuple sequence 
 for multidimensional indexing is deprecated; use \`arr\[tuple(seq)\]\` instead of \`arr\[seq\]\`. In the future this will
 be interpreted as an array index, \`arr\[np.array\(seq)\]\`, which will result either in an error or a different result.
 b = a\[a_slice\]_ - This warning came when using a function form the scipy package, this problem was fixed by just turning of this
 warning but this is not good for a long term fix and this problem might fix itself in newer versions of scipy

## Author
 - __Rinze-Pieter Jonker__ (rinze.pieter.jonker@gmail.com)
