#!/usr/bin/env python3

import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np
import pandas as pd

style.use("seaborn")


def cross_corr(y, x, lag):
    """
    This function calculates the cross correlation with lag

    :param y: Pandas Series, the first signal
    :param x: Pandas Series, the second signal
    :param lag: int, the amount of samples that you want to have as lag
    :return:  the cross correlation
    """
    return x.corr(y.shift(lag))


line1 = pd.Series(2*np.sin(np.arange(0, 10, 0.1)))
line2 = pd.Series(4*np.sin(np.arange(0, 10, 0.1)))
line3 = pd.Series(2*np.sin(np.arange(-3, 7, 0.1)))

plt.plot(line1, label="Sin Curve")
plt.plot(line2, label="Sin curve with higher amplitude")
plt.plot(line3, label="Sin cure with delay")
plt.title("Sinus curves", fontsize=22)
plt.legend()

plt.show()

cross1 = [cross_corr(line1, line2, i) for i in range(-100, 101, 5)]
cross2 = [cross_corr(line1, line3, i) for i in range(-100, 101, 5)]

plt.plot(list(range(-100, 101, 5)), cross1, label=" sin curve vs. sin curve with higher amplitude", c="green")
plt.plot(list(range(-100, 101, 5)), cross2, label=" sin curve vs. sin curve with delay", c="red")
plt.title("Cross-correlation between for evey sample", fontsize=22)
plt.ylabel("Correlation", fontsize=16)
plt.xlabel("Offset (Samples)", fontsize=16)
plt.axvline(0, c="black")
plt.legend(loc=4)
plt.ylim([-1.25, 1.25])
plt.show()
