#!/usr/bin/env python3
"""
This script creates a EmgSignal object that can be used to extract features that can be put into a machine learning
algorithm

Usage:
EmgSignal.py --patients <PATIENTS> --exercise <EXERCISES>

Parameters:
 -p, --patients: The patients used for the analysis
 -e, --exercise: The exercise / exercises used for this analysis

Optional Parameters:
 -h, --help     : This parameter will give you the help and exit the code
 -m, --modality : This parameter is to set the modality that is used to retrieve the data, default = 'IMU_EMG'
"""

# IMPORTS
import sys
import warnings
import pywt
import csv
import argparse
import yaml
import os
import time
import pickle

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from Scripts.extractData import loadEmgFeatures, loadImuEmgFeatures
from itertools import compress
from statsmodels.tsa.ar_model import AR
from math import sqrt
from scipy import signal
from sklearn.preprocessing import MinMaxScaler
from matplotlib import style


# GLOBALS
# Setting the style for matplotlib
style.use("seaborn")

# Retrieving globals from the config.yaml
with open("H:/My Documents/EMGAnalysis/config.yaml", 'r') as config:
    cfg = yaml.load(config)
    with open(cfg["diagnosis"]) as csv_file:
        csv_reader = csv.reader(csv_file)
        csv_lines = [x for x in csv_reader][1:]
        DIAGNOSIS = {}
        for line in csv_lines:
            DIAGNOSIS[line[0]] = line[1].strip(" ")

    with open(cfg["sensor_IMU_EMG"]) as csv_file:
        csv_reader = csv.reader(csv_file)
        csv_lines = [x for x in csv_reader][1:]
        IMU_EMG_NAMES = {}
        for line in csv_lines:
            IMU_EMG_NAMES[int(line[0])] = line[1].strip(" ")

    with open(cfg["sensor_EMG"]) as csv_file:
        csv_reader = csv.reader(csv_file)
        csv_lines = [x for x in csv_reader][1:]
        EMG_NAMES = {}
        for line in csv_lines:
            EMG_NAMES[int(line[0])] = line[1].strip(" ")

    COLORS = cfg["colors"]
    EXERCISES_WITHOUT = cfg["exercises_without"]
    FIGSIZE = cfg["figsize"]
    SAVE_LOCATION = cfg["plot_save_location"]

# Creating the save location if it doesn't exist
if not os.path.exists(SAVE_LOCATION):
    os.mkdir(SAVE_LOCATION)


# CODE
def cross_corr(y, x, lag):
    """
    This function calculates the cross correlation with lag

    :param y: Pandas Series, the first signal
    :param x: Pandas Series, the second signal
    :param lag: int, the amount of samples that you want to have as lag
    :return:  the cross correlation
    """
    return x.corr(y.shift(lag))


def ar_coefficients(windowSig, order):
    """
    Creates the Autoregressive model

    :param windowSig: the signal
    :param order:
    :return: the autoregressive coeffs
    """
    model = AR(windowSig)
    fitted = model.fit(maxlag=order)
    return fitted.params.tolist()


def calculate_ApEn(u, m, r):
    """
    calculates the Approximate entropy of a signal, this code was retrieved from:
    ://gist.githu.com/DustinAlandezes/a835909ffd15b9927820d175a48dee41

    :param u: int, the signal
    :param m: int, the dimension
    :param r: int, the threshold value
    :return: int, the approximate entropy
    """
    def _maxdist(x_i, x_j):
        return max([abs(ua - va) for ua, va in zip( x_i, x_j)])

    def _phi(m):
        x = [[u[j] for j in range(i, i + m - 1 + 1)] for i in range(n - m + 1)]
        c = [len([1 for x_j in x if _maxdist(x_i, x_j) <= r]) / (n - m + 1.0) for x_i in x]
        return (n - m + 1.0)**(-1) * sum(np.log(c))

    n = len(u)
    return abs(_phi(m + 1) - _phi(m))


class EmgSignal:
    """
    This class is made for analysis of EMG data and for the creation of features for machine learning

    :param modality: string, the modality that is used for the sensors
    :param exercise: int, the exercise id for the data
    :param patient: string, the id of the patient
    :param frame_rate: int, the amount of frames per second of the EMG data
    :param selected_sensors: array-like, the sensors that are used for this emg signal object
    :param window_size: int, the amount of samples in a window
    :param window_step: int, the amount of samples that are in each step
    :param low: int, the Hz amount that is used for the lowpass filtering
    """
    def __init__(self, modality, exercise, patient, frame_rate=2000, selected_sensors=(4, 6), window_size=3000,
                 window_step=333, low=(4, 30)):
        self.exercise = exercise
        self.patient = patient

        # Setting the diagnosis for the patient, if no diagnosis is found it will set the diagnosis to None
        try:
            self.diagnosis = DIAGNOSIS[patient]
        except KeyError:
            self.diagnosis = None

        self.modality = modality
        self.frame_rate = frame_rate
        self.selected_sensors = selected_sensors

        self.window_size = window_size
        self.window_step = window_step

        # Setting the right amount of sensors and the right muscle names for the modality
        if modality == "EMG":
            self.sensors = 6
            self.sensor_names = EMG_NAMES
        elif modality == "IMU_EMG":
            self.sensors = 8  # Only 8 sensors are used because the PIP sensors (9, 10) show only noise
            self.sensor_names = IMU_EMG_NAMES
        else:
            print("[ERROR] Modality '{}' is not supported".format(modality))

        # Retrieving the emg data
        self.raw_emg, self.rectified_filtered, self.mean_corrected = self.retrieve_emg(low=low)

    def retrieve_emg(self, low=(4, 30)):
        """
        This function retrieves the EMG signal from the .npy file given for the patient and exercise combination

        :param low: int, the amount of Hz that is used for the low bandpass filter
        :return: array-like, a tuple with the raw emg, the rectified emg and the mean corrected emg
        """
        # loading in the data from the right modality
        if self.modality == "IMU_EMG":
            emg = loadImuEmgFeatures(nemo_id=self.patient, exercise=self.exercise, downfactor=1)[1][self.patient]
        else:
            emg = loadEmgFeatures(nemo_id=self.patient, exercise=self.exercise, downfactor=1)[1][self.patient]

        # Setting up the filter
        b, a = signal.butter(2,  round(np.array(low) / self.frame_rate), btype="bandpass")

        # Extracting the data from the emg signal
        raw_emg = [emg[:, index] for index in self.selected_sensors]
        mean_corrected = [raw_emg[index] - np.mean(raw_emg) for index, _ in enumerate(raw_emg)]

        # Extracting the rectified emg signal and filtering it
        rectified = [abs(mean_corrected[index]) for index, _ in enumerate(mean_corrected)]

        rectified_filtered = [signal.filtfilt(b, a, rectified[index]) for index, _ in enumerate(rectified)]
        return raw_emg, rectified_filtered, mean_corrected

    def plot_filter_response(self, low=(4, 30)):
        """
        This function plots the filter response of the filter that is used for the rectified emg signal

        :param low: array-like, a array with the threshold values for the filter
        """
        # Setting save location
        save_location = "{}SignalPlots/".format(SAVE_LOCATION)
        if not os.path.exists(save_location):
            os.mkdir(save_location)

        # Setting the figure and the starting order name
        order = 0
        fig = plt.figure(figsize=FIGSIZE)

        # Retrieving all the filters for each order, the odd numbered orders are skipped
        for x, y in [signal.butter(item, np.array(low) / self.frame_rate, btype="bandpass") for item in range(0, 11, 2)]:
            w, h = signal.freqz(x, y, worN=self.frame_rate)
            plt.plot((self.frame_rate * 0.5 / np.pi) * w, abs(h),
                     label="Order = {}".format(order),
                     linewidth=0.75)
            order += 2

        # Setting the lines for the threshold values
        plt.axvline(low[0], c="green", linewidth=0.5)
        plt.axvline(low[1], c="green", linewidth=0.5)

        # Adding the labels and changing the layout
        plt.xlim(right=400)
        plt.title("Filter response", x=0, ha="left", fontsize=32)
        plt.xlabel("Hertz (Hz)", fontsize=16)
        plt.ylabel("Response", fontsize=16)
        plt.legend()

        fig.savefig(save_location + "Filter response.pdf")

        print("\t > Figure '{}.pdf' saved at location '{}'".format("Filter response", save_location))

    def plot_signal(self, signal_type="all", seconds="all"):
        """
        This function plots out the signal

        :param signal_type: string, this parameter
        :param seconds: array-like,
        :return:
        """
        # Creating a save directory
        save_location = "{}SignalPlots/".format(SAVE_LOCATION)
        if not os.path.exists(save_location):
            os.mkdir(save_location)

        # Setting the right signal type
        if signal_type == "raw":
            emg = self.raw_emg
        elif signal_type == "mean_corrected":
            emg = self.mean_corrected
        elif signal_type == "rectified":
            emg = self.rectified_filtered
        else:
            emg = self.raw_emg, self.mean_corrected, self.rectified_filtered

        # Checking if specific sensors are given
        sensors = self.selected_sensors

        fig, axes = plt.subplots(nrows=len(sensors), sharex=True, figsize=FIGSIZE)

        for count, row in enumerate(axes):
            if type(emg) == tuple:
                # Checking if a specific time is given and translating seconds to frames
                # If all is given it will take all samples
                if seconds == "all":
                    secs = 0, len(emg[0][count])
                else:
                    secs = np.array(seconds) * 2000

                signal_types = ["Raw", "Mean corrected", "Rectified Filterd"]

                for c, item in enumerate(emg):
                    y_axis = item[count][secs[0]: secs[1]]
                    duration = [x / 2000 for x in range(len(item[count]))][secs[0]: secs[1]]
                    row.plot(duration, y_axis, label=signal_types[c], linewidth=0.75)
                    row.legend(loc=4)
            else:
                # Checking if a specific time is given and translating seconds to frames
                # If all is given it will take all samples
                if seconds == "all":
                    secs = 0, len(emg[count])
                else:
                    secs = np.array(seconds) * 2000

                y_axis = emg[count][secs[0]: secs[1]]
                duration = [x / 2000 for x in range(len(emg[count]))][secs[0]: secs[1]]
                row.plot(duration, y_axis, c="black", linewidth=0.75)
            row.hlines(0, xmin=min(duration), xmax=max(duration), linestyles="--", colors="grey")
            row.set_title(IMU_EMG_NAMES[self.selected_sensors[count]], ha="left", fontsize=16, x=0)
            row.set_ylabel("EMG (A.U.)", fontsize=10)
            if count + 1 == len(sensors):
                row.set_xlabel("Time (Seconds)", fontsize=10)

        fig_title = "Signal plot of patient N{} with exercise {}".format(self.patient, self.exercise)
        fig.suptitle(fig_title, fontsize=32)
        fig.savefig("{}{}.pdf".format(save_location, fig_title))

        print("\t > Figure '{}.pdf' saved at location '{}'".format(fig_title, save_location))

    def cross_corr(self, lag=50, downsample_factor=16):
        """
        This function calculates the Cross correlation and auto correlation between the selected sensors

        :param lag: int or array-like, this wil define each offset for the cross correlations, if an int is given it
                    will create a range object of -<int> to <int> + 1
        :param downsample_factor: int, the factor that is used to downsample the data
        :return: array-like, the cross correlations with the given sensors as label
                            (<Cross_Correlations>, (<Sensor1>, <Sensor2>))
        """
        # Defining the lag range
        if type(lag) == int:
            lag_range = range(-lag, lag + 1)
        elif type(lag) == list or type(lag) == range:
            lag_range = lag

        results = []
        for y_index, y_sensor in enumerate(self.selected_sensors):
            # Retrieving the y_sensor data
            y_data = self.rectified_filtered[y_index]
            for x_index, x_sensor in enumerate(self.selected_sensors):
                # Retrieving the x_sensor data
                x_data = self.rectified_filtered[x_index]

                begin = 0
                end = begin + self.window_size

                # Calculating the different lagged cross correlations for sensor y against sensor x
                cross_correlations = []
                delays = []
                while end < x_data.size:
                    # Retrieving the y_window and x_window
                    y_window = pd.Series(signal.resample(y_data[begin:end],
                                                         int(len(y_data[begin:end]) / downsample_factor)))
                    x_window = pd.Series(signal.resample(x_data[begin:end],
                                                         int(len(x_data[begin:end]) / downsample_factor)))

                    # Calculating the cross correlation for each offset
                    window_corrs = [cross_corr(y_window, x_window, lag=offset) for offset in lag_range]

                    # Calculating the delay between the signals
                    delay_arr = np.linspace(-0.5*len(y_window) / self.frame_rate,
                                            0.5*len(x_window) / self.frame_rate,
                                            len(y_window))
                    delay = delay_arr[np.argmax(window_corrs)]
                    window_corrs.append(delay)

                    cross_correlations.append(window_corrs)

                    begin += self.window_step
                    end += self.window_step
                results.append([cross_correlations, (y_sensor, x_sensor), delays])
        return results

    def plot_cross_corr(self, lag=range(-100, 101, 5), downsample_factor=16):
        """
        This function will create a plot for each combination of the sensors, it will also include the auto-correlations

        :param lag: int or array-like, this wil define each offset for the cross correlations, if an int is given it
                    will create a range object of -<int> to <int> + 1
        :param downsample_factor: int, the factor that is used to down sample the data
        """
        # Setting the save location and creating the directories if they don't exist
        save_location = "{}CrossCorr/".format(SAVE_LOCATION)
        if not os.path.exists(save_location):
            os.mkdir(save_location)

        # Defining the lag_rang
        if type(lag) == int:
            lag_range = range(-lag, lag + 1)
        elif type(lag) == list or type(lag) == range:
            lag_range = lag

        # Retrieving all the cross correlations
        cross_corrs = self.cross_corr(lag=lag_range, downsample_factor=downsample_factor)

        # Setting the figure for the heatmaps, using the root of the lenght so that the amount of rows is always the same as the columns
        fig, ax = plt.subplots(nrows=int(sqrt(len(cross_corrs))),
                               ncols=int(sqrt(len(cross_corrs))),
                               figsize=FIGSIZE,
                               sharex=True,
                               sharey=True)
        fig.subplots_adjust(wspace=0.05,
                            hspace=0.05)
        fig_title = "Cross Correlations_exercise{}_patient_N{}".format(self.exercise, self.patient)
        fig.suptitle("Cross correlations between sensors for patient N{} with exercise {}".format(self.patient,
                                                                                                  self.exercise),
                     fontsize=32)
        # Generating the heatmaps for each cross correlation
        cross_count = 0
        no_cbar = True
        cbar_ax = fig.add_axes([0.92, 0.15, 0.04, 0.7])
        for row_counter, row in enumerate(ax):
            for col_counter, col in enumerate(row):
                plot = ax[row_counter, col_counter]

                # removing the delay out of the cross correlation
                data = cross_corrs[cross_count][0]
                data = [x[:-1] for x in data]

                y_ticks = []
                for count, x in enumerate(range(len(data))):
                    if count % 5 ==0:
                        y_ticks.append(x)
                    else:
                        y_ticks.append("")

                sns.heatmap(data,
                            ax=plot,
                            cbar=no_cbar,
                            cbar_ax=cbar_ax,
                            yticklabels=y_ticks)
                no_cbar = False

                # Adding a y label to every first plot in a row
                if col_counter == 0:
                    plot.set_ylabel("{}\n Windows".format(self.sensor_names[cross_corrs[cross_count][1][0]]),
                                    fontsize=22)

                # Adding a x label to every first plot in a column and changing its location
                if row_counter == 0:
                    plot.set_xlabel(self.sensor_names[cross_corrs[cross_count][1][1]], fontsize=22)
                    plot.xaxis.set_label_coords(0.5, 1.1)

                # Changing the x_tick labels for the last plots, the rest will don't have xtick_labels
                elif row_counter == int(sqrt(len(cross_corrs))) - 1:
                    current = [int(x.get_text()) for x in plot.get_xticklabels()]
                    new = [round(lag_range[x] * downsample_factor / self.frame_rate, 3)
                           for x in current]
                    plot.set_xticklabels(new, fontsize=10)
                    plot.set_xlabel("Offset (seconds)", fontsize=22)

                cross_count += 1

        fig.savefig("{}{}.pdf".format(save_location, fig_title))
        print("\t > Figure '{}.pdf' saved at location '{}'".format(fig_title, save_location))

    def fft_shift(self, downsample_factor=16):
        """
        This function is used to calculate the shift in the Fast Forier Transform, Not working and not used at the
        moment

        :param downsample_factor: int, the factor that is used to down sample the data
        :return: the shift in the FFT
        """
        results = []
        for y_index, y_sensor in enumerate(self.selected_sensors):
            y_data = self.rectified_filtered[y_index]
            for x_index, x_sensor in enumerate(self.selected_sensors):
                x_data = self.rectified_filtered[x_index]
                # Only calculating it once for each sensor combination since 4 x 6 is the same as 6 x 4
                if y_sensor <= x_sensor:

                    begin = 0
                    end = begin + self.window_size

                    shifts = []
                    while end < x_data.size:
                        # Retrieving the y_window and x_window
                        y_window = signal.resample(y_data[begin:end],
                                                   int(len(y_data[begin:end]) / downsample_factor))
                        x_window = signal.resample(x_data[begin:end],
                                                   int(len(x_data[begin:end]) / downsample_factor))
                        fft_y = np.fft.fft(y_window)
                        fft_x = np.fft.fft(x_window)

                        shifts.append(np.log(fft_x[1:] / fft_y[1:]) / -1.j*np.arange(1, len(x_window)))

                        begin += self.window_step
                        end += self.window_step
                    results.append(shifts)
        return results

    def extract_features(self):
        """
        This function retrieves the statistical features from the raw emg

        :return: list with all the features for each sensor
        """
        results = []
        for index, sensor in enumerate(self.selected_sensors):
            data = self.mean_corrected[index]

            # Setting up empty dictionary for each sensor
            features = {"MAV": [],
                        "RMS": [],
                        "VAR": [],
                        "WL": [],
                        "AR": [],
                        "coeff": [],
                        "hist": []}
            begin = 0
            end = begin + self.window_size

            # Running through the overlapping window
            while end < len(data):
                window = data[begin:end]

                # Histogram
                min = np.mean(window) - 3 * np.std(window)
                max = np.mean(window) + 3 * np.std(window)
                bins = np.linspace(min, max, 9)
                hist = np.histogram(window, bins)[0]
                nhist = hist / np.sum(hist)
                features["hist"].append(nhist)

                # Wavelet features
                coeffs = pywt.wavedec(window, "db7", level=3)[1:]
                features["coeff"].append([coeffs[0][0], coeffs[0][-1], np.mean(coeffs[0])])

                # Amplitude features
                features["MAV"].append(np.mean(np.square(window)))
                features["RMS"].append(np.sqrt(np.mean(np.square(window))))
                features["WL"].append(np.sum(np.abs(np.diff(window))))
                features["VAR"].append(np.var(window))

                # Auto-regression coefficents
                features["AR"].append(ar_coefficients(window, order=4))

                begin += self.window_step
                end += self.window_step
            results.append(features)
        return results

    def ml_output(self, lag=range(-100, 101, 5), downsample_factor=16, stat_feat=True, auto_reg=True, histogram=True,
                  coeff=True, cross_corr=True):
        """
        This function is used to generate all the data for a machine learning data set

        :param lag: int, list or range, these are the offsets ussed to calculate the cross correlations, if an int is
                         given it will generate a range of -<int> to <int> + 1 with a step of 1
        :param downsample_factor: int, the factor that is used to downsample the data
        :param stat_feat: boolean, this boolean decides if the statistical features are used in the data
        :param auto_reg: boolean, this boolean decides if the autoregressive coefficents are used in the data
        :param histogram: boolean, this boolean decides if the EMG histogram is used in the data
        :param coeff: boolean, this boolean decides if the marginals of the DWT are used in the data
        :param cross_corr: boolean, this boolen deiceds if the cross correlations are used in the data
        :return: pandas DataFrame, all the data for this EmgSignal object
        """
        data = pd.DataFrame()

        # Retrieving all the cross correlations
        if type(lag) == int:
            lag_range = range(-lag, lag + 1)
        elif type(lag) == range or type(lag) == list:
            lag_range = lag

        # If the cross_corr parameter is set to true it wil add the cross correlation to the features used for machine
        # learning
        if cross_corr:
            for count, item in enumerate(self.cross_corr(lag=lag_range, downsample_factor=downsample_factor)):
                if data.empty:
                    data = pd.DataFrame(item[0])

                    columns = ["{} {}".format(index, item[1]) for index in lag_range]
                    columns.append("{} delay".format(item[1]))

                    data.columns = columns

                else:
                    # Only adding the cross correlations one time
                    if item[1][0] == item[1][1] or item[1][1] - item[1][0] == 2:
                        new_df = pd.DataFrame(item[0])

                        columns = ["{} {}".format(index, item[1]) for index in lag_range]
                        columns.append("{} delay".format(item[1]))

                        new_df.columns = columns

                        data = pd.concat([data, new_df], axis=1)

            # # Saving the column names for the new df
            # columns = data.columns

            # Taken out of the script because it created a bigger patient artifact and added a delay, yet it did improve
            # the accuracy of the classifier
            # # Averaging out the cross correlations to make time play a roll
            # # Setting the step size for the averaging
            # step_size = 1
            # window_size = 15
            #
            # start = 0
            # end = start + window_size
            #
            # new = []
            # while start < data.shape[0]:
            #     if end > data.shape[0]:
            #         window_df = data.iloc[start:]
            #     else:
            #         window_df = data.iloc[start:end]
            #     new.append(window_df.mean().tolist())
            #
            #     start += step_size
            #     end += step_size
            #
            # data = pd.DataFrame(new)
            # data.columns = columns

        # Adding the Statistical features
        features = self.extract_features()

        # Adding the features for each sensor
        for index, sensor in enumerate(self.selected_sensors):
            if stat_feat:
                data["MAV {}".format(sensor)] = features[index]["MAV"]
                data["RMS {}".format(sensor)] = features[index]["RMS"]
                data["VAR {}".format(sensor)] = features[index]["VAR"]
                data["WL {}".format(sensor)] = features[index]["WL"]

            if auto_reg:
                for count in range(len(features[index]["AR"][0])):
                    data["AR{} {}".format(count, sensor)] = [x[count] for x in features[index]["AR"]]

            if histogram:
                for count in range(len(features[index]["hist"][0])):
                    data["hist{} {}".format(count, sensor)] = [x[count] for x in features[index]["hist"]]

            if coeff:
                for count, column in enumerate(["coeff left margin {}".format(sensor),
                                                "coeff right margin {}".format(sensor),
                                                "coeff mean {}".format(sensor)]):
                    data[column] = [x[count] for x in features[index]["coeff"]]

        data["diagnosis"] = [self.diagnosis for _ in range(data.shape[0])]
        data["patient"] = ["N{}".format(self.patient) for _ in range(data.shape[0])]
        return data

    def __str__(self):
        return "EmgSignal object of patient N{} with exercise {} and modality {}".format(self.patient,
                                                                                         self.exercise,
                                                                                         self.modality)


def argument_parser():
    """
    This function parses the commandline and returns all the information gained from the commandline
    :return: tuple, a tuple containing all the commandline arguments (Exercises, Patients, Modality)
    """
    parse = argparse.ArgumentParser(description="This script is used to generate plots and data for emg signals")
    parse.add_argument("-e", "--exercises",
                       help="The exercise or exercises that are going to be used for the analysis",
                       required=True,
                       nargs="+")
    parse.add_argument("-p", "--patients",
                       help="The patient and patients that are going to be used for the analysis. "
                            "Diagnosis of the patient needs ot be in the 'diagnosis.csv' file",
                       required=True,
                       nargs="+")
    parse.add_argument("-m", "--modality",
                       help="The modality that is going to be used. "
                            "Only 'IMU_EMG' and 'EMG' are supported at the moment",
                       required=False,
                       default="IMU_EMG")
    args = parse.parse_args()

    # Turning all the patient ID's into the right format
    string_list = [str(x) for x in list(range(10))]
    patients = ["0{}".format(x) if x in string_list else x for x in args.patients]

    # Turning all the exercise ID's into the right datatype
    exercises = [int(x) for x in args.exercises]
    return exercises, patients, args.modality


def retrieve_data(patients, exercise, modality, filename=None, overwrite=False, selected_sensors=[4, 6], low=[4, 30],
                  window_step=333, window_size=999, normalize=True):
    """
    This function retrieves all the features from all the patients of one exercise and saves them to a csv file,
    if this .csv file already exists then it will load this one in, instead of calculating all the features.

    :param patients: array-like, this is an array like with all the patient ids that are going to be used to
                     extract the data with
    :param exercise: int, the exercise that is going to be used to load in the data
    :param modality: string, the modality that is going to be used to load in the data
    :param filename: string, the file location of the data file
    :param overwrite: boolean, if this boolean is set to true it will overwrite the data file
    :param selected_sensors: array-like, the sensors that are going to be used to load in the data
    :param low: int, the amount of Hz that the low band pass filter is going to use
    :param window_step: int, the size of the step that is used for the overlapping windows
    :param window_size: int, the size of the window that is used for the overlapping windows
    :param normalize: boolean, this boolean decides if the data is going to be normalized or not
    :return: pandas DataFrame with all the features for each window and patient
    """
    # Checking if a file is given and if it is given if it exists
    if filename is None:
        file_exists = True
        filename = "../Data/Data_Exercise_{}_Default.csv".format(exercise)
    else:
        file_exists = os.path.exists(filename)

    if file_exists and not overwrite:
        # Reading the data in from a file if there is already a file
        data = pd.read_csv(filename)
        exists = True

        # Checking if all the patients are in the file
        patients_in_data = data["patient"].unique()
        for patient in patients:
            patient = "N" + patient
            # if a patient is missing it will generate a new file
            if patient not in patients_in_data:
                print("[ERROR] Not all patients are saved in the data file, generating a new file")
                exists = False
                break
    else:
        exists = False

    if not exists:
        # Setting up the loadbar
        load_bar_width = 50

        sys.stdout.write(" > Generating the EmgSignal objects and creating a file at location {} \n"
                         "   [%s]".format(filename) % (" " * load_bar_width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (load_bar_width + 1))
        print_amount = round(load_bar_width / len(patients))

        data = pd.DataFrame()

        # Creating the patient EmgSignal objects and saving them in the data DataFrame
        for patient in patients:
            sig = EmgSignal(modality=modality,
                            patient=patient,
                            exercise=exercise,
                            selected_sensors=selected_sensors,
                            low=low,
                            window_size=window_size,
                            window_step=window_step)

            if data.empty:
                data = sig.ml_output(lag=range(-100, 101, 5))
            else:
                data = pd.concat([sig.ml_output(lag=range(-100, 101, 5)), data], ignore_index=True)

            # Updating the load bar
            sys.stdout.write("#" * print_amount)
            sys.stdout.flush()
        sys.stdout.write("]\n")

        # Normalizing the all the statistical features and saving the data
        if normalize:
            data = normalize_features(data=data, overwrite=True, exercise=exercise,
                                      selected_features=["MAV", "RMS", "VAR", "WL", "AR", "hist", "coeff"])
        data.to_csv(filename)

    # Removing the index from the data
    if "Unnamed: 0" in data:
        data = data.drop("Unnamed: 0", axis=1)

    return data


def normalize_features(data, selected_features=("WL", "MAV", "VAR", "RMS"), overwrite=False, exercise=""):
    """
    This function normalizes the selected features with the already existing scalers.
    If there are no scalers it will create the scalers with the data that is given to normalize.

    :param data: Pandas DataFrame, this is the data wich you want to normalize
    :param selected_features: array-like, these are the features that are going to be normalized
    :param overwrite: boolean, if the scalers that al ready exist will be overwritten to create new scalers
    :param exercise: string or int, the exercise used for the data
    :return: Pandas DataFrame, the same as the input but now with normalized features
    """
    # Setting the save location and creating the directories
    scaler_location = "../Data/Scalers/"
    if not os.path.exists(scaler_location):
        os.mkdir(scaler_location)

    for feature in selected_features:
        col = [str(cols) for cols in data.columns if str(cols).startswith(feature)]
        to_normalize = data[col].values

        scaler_path = "{}{}_{}.joblib".format(scaler_location, feature, exercise)
        if os.path.exists(scaler_path) and not overwrite:
            # Loading in the scaler and transforming the feature
            with open(scaler_path, "rb") as f:
                scaler = pickle.load(f)
                try:
                    normalized = scaler.transform(to_normalize)
                    new_scaler = False
                except ValueError:
                    print("[ERROR] Scaler did not fit to Data, generating new scaler at location {}".format(scaler_path))
                    scaler = MinMaxScaler()
                    normalized = scaler.fit_transform(to_normalize)
                    new_scaler = True

            if new_scaler:
                with open(scaler_path, "wb") as f:
                    pickle.dump(scaler, f)

        else:
            # Creating the scaler and fit transforming the feature
            scaler = MinMaxScaler()
            normalized = scaler.fit_transform(to_normalize)
            with open(scaler_path, "wb") as f:
                pickle.dump(scaler, f)

        # Inserting the new data
        for index, cols in enumerate(col):
            data[cols] = normalized[:, index]
    return data


def plot_features_box(data, selected_features=["MAV"], to_add="", modality="IMU_EMG"):
    """
    This functions plots the given features in a boxplot

    :param data: pandas DataFrame, the data that is going to be used
    :param selected_features: array-like, this array-like contians all the features that are going to be used
    :param to_add: string, text that will be added at the end of the plot title and save name
    :param modality: string, the modality that is used for the data, not supported yet
    """
    # Setting the full names of the features
    feature_names = {"MAV": "Mean Average Value",
                     "RMS": "Root Mean Square",
                     "VAR": "Variance",
                     "WL": "Waveform Length"}

    # Setting the save location and checking if it exists
    save_location = "{}Boxplots/".format(SAVE_LOCATION)
    if not os.path.exists(save_location):
        os.mkdir(save_location)

    # Making a different plot for each feature
    for feature in selected_features:
        # retrieving the columns containing the data
        features_col = [x for x in data.columns if feature in x]

        # Generating the figure, each muscle gets its own subplot
        boxplots, ax = plt.subplots(ncols=len(features_col),
                                    figsize=FIGSIZE)

        for col_count, plot in enumerate(ax):
            # Parsing the data DataFrame
            feature_data = pd.DataFrame(data[features_col[col_count]])
            feature_data["diagnosis"] = data["diagnosis"]

            # Generating all the data for each diagnosis
            boxplot_data = []
            for diagnosis in feature_data["diagnosis"].unique():
                boxplot_data.append(feature_data[features_col[col_count]].loc[feature_data["diagnosis"] == diagnosis])

            # Plotting the data
            boxplot = plot.boxplot(boxplot_data,
                                   patch_artist=True)
            plot.set_xticklabels(feature_data["diagnosis"].unique(), fontsize=22)

            for count, box in enumerate(boxplot["boxes"]):
                box.set_facecolor(COLORS[count])
                # Workaround for adding alpha to the colors
                r, g, b, a = box.get_facecolor()
                box.set_facecolor((r, g, b, 0.3))

            plot.set_ylabel(feature, fontsize=28)
            plot.set_xlabel("Diagnoses", fontsize=28)
            plot.set_xticklabels(plot.get_xticklabels(), size=28)

            # Selecting the right dictionary to select the muscle names
            if modality == "EMG":
                muscle_dict = EMG_NAMES
            else:
                muscle_dict = IMU_EMG_NAMES

            subplot_title = "{}".format(muscle_dict[int(features_col[col_count][-1])])
            plot.set_title(subplot_title, fontsize=28)

        # Adding the title and saving the plot
        try:
            fig_title = "{} ".format(feature_names[feature]) + to_add
        except KeyError:
            fig_title = "{} ".format(feature) + to_add

        boxplots.suptitle(fig_title, fontsize=64, y=0.99)
        boxplots.savefig(save_location + fig_title + ".pdf")
        print("\t > Figure '{}.pdf' saved at location '{}'".format(fig_title, save_location))


def plot_features_scatter(data, selected_features=("MAV", "RMS", "WL", "VAR"), to_add="", modality="IMU_EMG",
                          selected_sensors=(4, 6)):
    """
    This function graphs out the given features in a scatterplot between the two selected sensors

    :param data: pandas DataFrame, the feature data that will be used for the graphs
    :param selected_features: array-like, the features that you want a graph of
    :param to_add: string, extra text that will be added
    :param modality: string, the modality used for the data
    :param selected_sensors: array-like, an array-like with a length of 2, these are the sensor id's that are use for
                                         the x and y axis
    :return: 2 scatter plots, one that is labeled based on diagnosis and one that is labeled based on patient
    """

    # Setting the full names of the features
    feature_names = {"MAV": "Mean Average Value",
                     "RMS": "Root Mean Square",
                     "VAR": "Variance",
                     "WL": "Waveform Length"}

    # Checking the modality and setting the muscle names
    if modality == "IMU_EMG":
        muscle_dict = IMU_EMG_NAMES
    else:
        muscle_dict = EMG_NAMES

    # Setting save location and making the directory if it does not exists
    save_location = "{}ScatterPlots/".format(SAVE_LOCATION)
    if not os.path.exists(save_location):
        os.mkdir(save_location)

    # Generating the figures for both plots
    diagnosis_based, ax = plt.subplots(nrows=1,
                                       ncols=len(selected_features),
                                       figsize=FIGSIZE)

    patient_based, ax2 = plt.subplots(nrows=1,
                                      ncols=len(selected_features),
                                      figsize=FIGSIZE)

    # Generating a graph for each feature
    for count, feature in enumerate(selected_features):
        features_col = [x for x in data.columns if feature in x]

        # Parcing out the x label and the y label
        y_axis_name = list(compress(features_col, [str(selected_sensors[0]) in x for x in features_col]))[0]
        x_axis_name = list(compress(features_col, [str(selected_sensors[1]) in x for x in features_col]))[0]

        # Retrieving the groups
        diagnoses = data["diagnosis"]
        patients = data["patient"]

        # Retrieving the data for the y and x axis
        y_axis = list(data[y_axis_name])
        x_axis = list(data[x_axis_name])

        # Trying to parse the Axis matplotlib object, yet is there is only one feature given this is not possible
        try:
            diagnosis_plot = ax[count]
            patient_plot = ax2[count]

        except TypeError:
            diagnosis_plot = ax
            patient_plot = ax2

        # Setting the points for each different diagnosis
        for c, diagnosis in enumerate(diagnoses.unique()):
            index = [x == diagnosis for x in diagnoses]
            y_data = list(compress(y_axis, index))
            x_data = list(compress(x_axis, index))

            diagnosis_plot.scatter(y_data, x_data, c=COLORS[c], label=diagnosis)

        # Setting the points for each different patient
        for c, patient in enumerate(patients.unique()):
            index = [x == patient for x in patients]
            y_data = list(compress(y_axis, index))
            x_data = list(compress(x_axis, index))

            patient_plot.scatter(y_data, x_data, c=COLORS[c], label=patient)

        # Trying to set the plot name with the right feature name, if it is not in the feature_names dict it will just
        # take the feature name given
        try:
            diagnosis_plot.set_title(feature_names[feature], fontsize=16, ha="left", x=0)
            patient_plot.set_title(feature_names[feature], fontsize=16, ha="left", x=0)
        except KeyError:
            diagnosis_plot.set_title(feature, fontsize=16, ha="left", x=0)
            patient_plot.set_title(feature, fontsize=16, ha="left", x=0)

        # Setting x label and y label for both plots
        diagnosis_plot.set_xlabel("{}".format(muscle_dict[int(x_axis_name[-1])]), fontsize=16)
        diagnosis_plot.set_ylabel("{}".format(muscle_dict[int(y_axis_name[-1])]), fontsize=16)
        diagnosis_plot.legend(fontsize=12)

        patient_plot.set_xlabel("{}".format(muscle_dict[int(x_axis_name[-1])]), fontsize=16)
        patient_plot.set_ylabel("{}".format(muscle_dict[int(y_axis_name[-1])]), fontsize=16)
        patient_plot.legend(fontsize=12)

    # Setting savename and plot title
    patient_plot_title = "Scatterplots of the different features grouped by patients" + to_add
    diagnosis_plot_title = "Scatterplots of the different features grouped by diagnosis" + to_add

    diagnosis_based.suptitle(diagnosis_plot_title, fontsize=32)
    patient_based.suptitle(patient_plot_title, fontsize=32)

    diagnosis_based.savefig(save_location + diagnosis_plot_title + ".pdf")
    patient_based.savefig(save_location + patient_plot_title + ".pdf")

    print("\t > Figure '{}.pdf' saved at location '{}'".format(patient_plot_title, save_location))
    print("\t > Figure '{}.pdf' saved at location '{}'".format(diagnosis_plot_title, save_location))


def calculate_correlations(data, selected_features=("MAV", "RMS", "WL", "VAR", "coeff", "hist", "AR"),
                           diagnoses=("Healthy", "Tremor", "Myoclonus")):
    """
    This function calculates the correlations between the features and returns 2 graphs, the first one with is a heatmap
    with all the data from all the diagnoses and the second is a figure with a heatmap with the correlations for each
    diagnosis

    :param data: pandas DataFrame, The data that is used for the analysis
    :param selected_features: array-like, an array-like with the features that are used for the correlations
    :param diagnoses: array-like, an array-like with all the diagnoses that are going to be used
    :return:
    """
    # Setting the savelocation
    save_loc = "{}FeatureCorr/".format(SAVE_LOCATION)
    if not os.path.exists(save_loc):
        os.mkdir(save_loc)

    # Extracting only the selected features from the dataset
    columns = []
    for feature in selected_features:
        for column in data.columns:
            if feature in column:
                columns.append(column)
    fixed_data = data[columns]

    # Creating a heatmap for each diagnosis
    # Setting the parameters
    corr_split_title = "Correlations between the features for each diagnosis"
    corr_split, axes = plt.subplots(ncols=len(diagnoses),
                                    nrows=1,
                                    figsize=FIGSIZE)
    corr_split.suptitle(corr_split_title, fontsize=32)
    plt.subplots_adjust(wspace=0.35)

    # Adding one colorbar for all figures in the plot
    cbar = True
    cbar_ax = corr_split.add_axes([0.92, 0.15, 0.04, 0.7])

    for count, diagnosis in enumerate(diagnoses):
        # Generating an list with bools that can be used as index
        index = [True if x == diagnosis else False for x in data["diagnosis"]]

        # parsing only the data of one diagnosis out of the dataframe and calculating the corretions
        corr = fixed_data[index].corr()

        sns.heatmap(corr, ax=axes[count], cbar=cbar, cbar_ax=cbar_ax)
        cbar = False
        axes[count].set_title(diagnosis,
                              ha="left",
                              x=0,
                              fontsize=16)

    # Creating a correlation heatmap for all diagnoses
    all_corr_title = "Correlations between the features for all the diagnoses"
    correlations = fixed_data.corr()
    all_corr = plt.figure(figsize=FIGSIZE)
    ax = all_corr.add_subplot(111)
    sns.heatmap(correlations, ax=ax)
    plt.title(all_corr_title, fontsize=32)

    # Saving the created figures
    corr_split.savefig("{}{}.pdf".format(save_loc, corr_split_title))
    all_corr.savefig("{}{}.pdf".format(save_loc, all_corr_title))
    print(" > Figure '{}.pdf' saved at location '{}'".format(corr_split_title, save_loc,))
    print(" > Figure '{}.pdf' saved at location '{}'".format(all_corr_title, save_loc,))


def main():
    exercises, patients, modality = argument_parser()
    print(" > Running code with the exercises " + str(exercises).strip("[]") +
          " for patients {}".format(patients) +
          " with modality {}".format(modality))

    # ignoring all the scipy warnings
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")

        # Running the analysis for every exercise separate
        for counter, exercise in enumerate(exercises):
            # Collecting all the signal objects for each task
            print(" > Retrieving all the EmgSignals for exercise {}".format(exercise))
            data = retrieve_data(patients=patients, exercise=exercise, modality=modality, low=[4, 30], window_step=333,
                                 window_size=3000, selected_sensors=[4, 6], normalize=False, overwrite=False)

            plot_features_box(data=data,
                              selected_features=["MAV", "RMS", "VAR", "WL"],
                              to_add="Not Normalized, exercise " + str(exercise))
            plot_features_scatter(data=data, to_add=" (not normalized), exercise " + str(exercise))

            data = normalize_features(data, selected_features=["MAV", "RMS", "VAR", "WL"],
                                      exercise=exercise,
                                      overwrite=False,
                                      to_add="Normalized, exercise " + str(exercise))

            plot_features_box(data=data,
                              selected_features=["MAV", "RMS", "WL", "VAR"])

            plot_features_scatter(data=data, to_add=" (normalized), exercise " + str(exercise))

            calculate_correlations(data=data)

            # Generating the cross correlations
            signals = [EmgSignal(patient=x,
                                 exercise=exercise,
                                 modality=modality,
                                 selected_sensors=[4, 6],  # 0, 2,
                                 window_size=3000,
                                 window_step=333,
                                 low=[4, 30]) for x in patients]

            signals[0].plot_filter_response()
            for sig in signals:
                sig.plot_cross_corr(lag=range(-100, 101, 2), downsample_factor=16)

            EmgSignal(patient="24",
                      exercise=22,
                      modality="IMU_EMG",
                      selected_sensors=[4, 6],
                      window_step=333,
                      window_size=3000,
                      low=[4, 30]).plot_signal(signal_type="all")

            EmgSignal(patient="17",
                      exercise=22,
                      modality="IMU_EMG",
                      selected_sensors=[4, 6],
                      window_step=333,
                      window_size=3000,
                      low=[4, 30]).plot_signal(signal_type="all")
    return 0


if __name__ == "__main__":
    start = time.time()
    print(" > Code started on {}".format(time.asctime()))
    main()
    print(" > Code finished, took {} seconds".format(round(time.time() - start)))
