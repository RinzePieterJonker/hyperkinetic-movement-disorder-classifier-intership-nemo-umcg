# IMPORTS
import sys
import yaml

import numpy as np

from scipy.signal import decimate

# METADATA
__author__ = "Joost Calon"
__status__ = "Working"

# GLOBALS
with open("H:/My Documents/EMGAnalysis/config.yaml", 'r') as config:
    cfg = yaml.load(config)
    DATA_PATH = cfg["data_path"]
    RAW_FOLDER = cfg["raw_path"]
    LOG_FILE = cfg["log_file"]


# CODE
def log(content, meta):
    with open(LOG_FILE, "a+") as file:
        file.write("[" + meta + "]\t" + content + "\n")

    print("[ERROR]", content)


def loadModality(nemo_id, exercise, modality, downfactor):
    """
    This function loads the Emg signal form a .npy file

    :param nemo_id: Str, the ID of the  patient
    :param exercise: Int, the ID of the exercise
    :param modality: Str, the type of Emg that you want to load
    :param downfactor: Int, the way the data is going to be loaded
    :return: Array-like, a Array matrix with 8 columns and N samples rows
    """
    task_vectors = {}
    task_timestamp = {}

    try:
        # Load
        filename = "N" + nemo_id + "_" + modality + "_" + str(exercise) + ".npy"

        frame_vector = np.load(DATA_PATH + RAW_FOLDER + filename, allow_pickle=True)

        # Decimate
        if downfactor > 1:
            frame_vector = decimate(np.asarry(frame_vector).T, downfactor).T

        # Append
        task_vectors[nemo_id] = frame_vector
    except:
        log("Unexpected error loading " + filename + ": " + str(sys.exc_info()[0]), "ERROR")
    return task_timestamp, task_vectors


def loadEmgFeatures(nemo_id, exercise, downfactor):
    return loadModality(nemo_id, exercise, "EMG", downfactor)


def loadImuEmgFeatures(nemo_id, exercise, downfactor):
    return loadModality(nemo_id, exercise, "IMU_EMG", downfactor)
