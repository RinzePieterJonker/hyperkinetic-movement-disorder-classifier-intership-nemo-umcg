#!/usr/bin/env python3
"""
This script can be used as a classifier for the NEMO data, it also trains and validates the OneVsOne and MetaOneVsOne
classifiers.

Usage:
OneVsOne.py --patients <PATIENTS ...> --exercise <EXERCISE ...>

Parameters:
 -p, --patients: the patients that are used for the training and classification of the classifier
 -e, --exercise: the exercise or exercise that is used for the creation of the object, if more than one exercise is
                 given it will create multiple classifiers

Optional Parameters:
 -h, --help     : this parameter shows the help of the arguments parser and will show the usage
 -m, --modality : this is the modality that used for the training and validation of the classifier and is also used for
                  the prediction of patients. Only 'IMU_EMG' and 'EMG' are supported and the default is 'IMU_EMG'
 -s, --step     : this is the size of the step between the overlapping windows, default = 333
 -n, --size     : this is the size of the overlapping windows, default = 3000
 -f, --filter   : this is the amount of Hz used for the low bandpass filter before the cross correlation, default = 20
 -r, --runs     : this is the amount of runs the code is going to do, default = 1
 -o, --overwrite: if this boolean is set to true it will overwrite the existing files, default = False
 -P, --predict  : the patients of which you want a prediction video and prediction, default = []
 -S, --sensors  : The sensors tha are used for the classification, training and validation of the classifier,
                  default = [4, 6]
"""

# IMPORTS
import time
import yaml
import csv
import warnings
import argparse
import os
import cv2
import sys
import gc
import glob

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

from collections import Counter
from matplotlib import style
from Scripts.OneVsAll import OneVsAll
from Scripts.EmgSignal import EmgSignal, normalize_features, retrieve_data
from sklearn.metrics import classification_report, roc_auc_score, roc_curve, confusion_matrix
from sklearn.preprocessing import label_binarize
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split

# METADATA
__author__ = "Rinze-Pieter Jonker"
__version__ = 1.1
__status__ = "Working"

# GLOBALS
# Setting the style for the matplotlib plots
style.use("seaborn")

# Retrieving the globals form the config file
with open("../config.yaml", "r") as config:
    cfg = yaml.load(config)
    FIGSIZE = cfg["figsize"]
    SAVE_LOCATION = cfg["plot_save_location"]

    with open(cfg["diagnosis"]) as csv_file:
        csv_reader = csv.reader(csv_file)
        csv_lines = [x for x in csv_reader][1:]
        DIAGNOSIS = {}
        for line in csv_lines:
            DIAGNOSIS[line[0]] = line[1].strip(" ")


# CODE
class MetaOneVsOne(OneVsAll):
    """
    This class is used as a meta classifier for OneVsOne classifiers and can train and validate the classifier. It can
    also classify patients from the NEMO data set

    This class uses the create_model, load_model and run function from the OneVsAll class

    Known issues:
    - The labels of the MetaOneVsOne classifier doesn't follow the the same order as the labels of the OneVsOne labels
      this problem can be fixed by switching the last labels around

    :param data: pandas DataFrame, this is the data that is used for the training of the OneVsOne classifiers
    :param name: string, this is the name given to the classifier and is used to make a save name for the trained
                         classifier
    :param exercise: int, this is the exercise that is used for the training of the classifier
    :param train: array-like, this is an array-like with all the patients that are in the train data set
    :param validation: array-like, this is an array-like with all the patients  that are in the validation data set
    :param labels: array-like, this array-like contains all the labels that are used in the
                               classification of the classifier
    :param classifiers: array-like, this array-like contains all the OneVsOne classifiers
    :param model: skLearn classifiers function, this is the model that is used for the classification of the patients
    :param modality: string, the modality that is used for the training of the classifier and for the classification of
                             patients
    :param selected_sensors: array-like, this array-like contains all the sensors that are used for the training and
                                         validation of the classifier
    :param window_size: int, this is the size of the windows that is used for the training of the classifier
    :param window_step: int, this is the size of the steps between the window that is used for the training of the
                             classifier
    :param low: int, this is the amount of Hz used for the filter of the cross correlation
    """
    def __init__(self, data, name, exercise, train, validation, labels, classifiers, model=MLPClassifier(),
                 modality="IMU_EMG", selected_sensors=[4, 6], window_size=999, window_step=333, low=20):
        self.data = data
        self.name = name
        self.exercise = exercise
        self.save_name = "../Data/Model/{}.joblib".format(name)
        self.train = train
        self.validation = validation
        self.labels = labels
        self.classifiers = classifiers
        self.model = model
        self.modality = modality
        self.selected_sensors = selected_sensors
        self.window_size = window_size
        self.window_step = window_step
        self.low = low
        self.has_run = False
        pass

    def retrieve_predictions(self, patients):
        """
        This function retrieves the predictions from the OneVsOne classifiers and saves it in a pandas dataframe

        :param patients: array-like, the patients that are going to be used for the predictions
        :return: a pandas DataFrame with the predictions made with the OneVsOne classifiers and with the true labels
        """
        predictions = pd.DataFrame()
        for patient in patients:
            patient = patient.strip("N")
            patient_df = pd.DataFrame()

            # retrieving all the predictions of one OneVsOne classifier, at the moment retruning the probabilities
            for counter, clf in enumerate(self.classifiers):
                pred = clf.predict(patient)[2]
                pred = pd.DataFrame(pred, columns=["{}.{}".format(clf.label_a, counter),
                                                   "{}.{}".format(clf.label_b, counter)])
                # Appending the predictions to the patient dataframe
                if patient_df.empty:
                    patient_df = pred
                else:
                    patient_df = patient_df.join(pred)
            patient_df["diagnosis"] = [DIAGNOSIS[patient] for _ in range(patient_df.shape[0])]

            # Combining all the patient DataFrames
            if predictions.empty:
                predictions = patient_df
            else:
                predictions = pd.concat([predictions, patient_df], ignore_index=True)
        return predictions

    def generate_data(self, overwrite=False):
        """
        This function generates all the data for the machine learning algorithm and will create a .csv file to make the
        classifier faster for training and validation in the future

        :param overwrite: boolean, if this boolean is set to True it will overwrite the existing file
        :return: array-like, a test set and a validation set, each set has 2 items. One is the data without the label
                             and the other are the labels
        """
        # Allocating the names for the training and validation files
        train_location = "../Data/Train_{}.csv".format(self.name)
        validation_location = "../Data/Validation_{}.csv".format(self.name)

        # Loading in the files if the file exist and if overwrite is False
        if os.path.exists(train_location) and os.path.exists(validation_location) and not overwrite:
            # removing the index while loading in the data
            train = pd.read_csv(train_location).drop(["Unnamed: 0"], axis=1)
            validation = pd.read_csv(validation_location).drop(["Unnamed: 0"], axis=1)

        # Generating and saving the data if the files don't exist or if overwrite is set to True
        else:
            train = self.retrieve_predictions(self.train)
            validation = self.retrieve_predictions(self.validation)

            train.to_csv(train_location)
            validation.to_csv(validation_location)

        # Splitting up the train and validation data set
        train = [train.drop(["diagnosis"], axis=1), train["diagnosis"]]
        validation = [validation.drop(["diagnosis"], axis=1), validation["diagnosis"]]
        return train, validation

    def evaluate_model(self, model, train, validation, print_results=False, plot_results=False):
        """
        This function evaluates the model and returns the evaluation plot and metrics

        :param model: trained skLearn Classifier object, this is the model that is used for the main classification
        :param train: array-like, an array-like with a dataframe with all the data and a list with the labels
        :param validation: array-like, an array-like with a dataframe with all the data and a list with the labels
        :param print_results: boolean, if this boolean is True, the function will print to the commandline
        :param plot_results: boolean, if this boolean is True, the function will show the plots at the end
                             of the function
        :return: the classifications of the train data and the validation data
        """
        # Creating a save location
        save_location = "{}/ClassifierPlots/".format(SAVE_LOCATION)
        if not os.path.exists(save_location):
            os.mkdir(save_location)

        if print_results:
            print("\t > Evaluating model {}".format(self.name))

        # Retrieving labels and predictions
        val_pred = model.predict(validation[0])
        val_proba = model.predict_proba(validation[0])
        val_true = validation[1]

        target_names = self.labels

        # Generating a feature importance plot if a tree classifier is used
        try:
            feat_imp = pd.Series(model.feature_importances_, list(train[0])).sort_values(ascending=False)[:30]

            feat_plot = plt.figure(figsize=FIGSIZE)
            axes = feat_plot.add_subplot(111)

            feat_imp.plot(kind="barh", ax=axes)
            axes.set_title("Feature importance of {}".format(self.name),
                           ha="left",
                           x=0)
            feat_plot.savefig("{}Featplot_{}.pdf".format(save_location, self.name))

            if not plot_results:
                plt.close()

        except AttributeError:
            if print_results:
                print("\t [WARNING] Couldn't generate feature importance for this model, code will skip this part")

        # Confusion Matrix
        conf = confusion_matrix(val_true, val_pred)

        conf_matrix = plt.figure(figsize=FIGSIZE)
        ax = conf_matrix.add_subplot(111)

        sns.heatmap(conf,
                    ax=ax,
                    fmt="d",
                    annot=True,
                    yticklabels=target_names,
                    xticklabels=target_names
                    )
        ax.set_ylabel("True label")
        ax.set_xlabel("Predicted label")
        ax.set_title("Confusion matrix of {}".format(self.name))

        conf_matrix.savefig("{}Confusion matrix for {}.pdf".format(save_location, self.name))

        if not plot_results:
            plt.close()

        # ROC curve
        roc = plt.figure(figsize=FIGSIZE)
        ax = roc.add_subplot(111)
        # ax.plot([0, 1], [0, 1], c="black", linestyle="--", linewidth=0.75)

        binary_labels = label_binarize(val_true, classes=self.labels)
        for i, label in enumerate(self.labels):
            fpr, tpr, _ = roc_curve(binary_labels[:, i], val_proba[:, i])

            ax.plot(fpr, tpr, label="AUC Score of {} = {}".format(label,
                                                                  roc_auc_score(binary_labels[:, i], val_proba[:, i])))
            ax.set_ylabel("TPR", fontsize=16)
            ax.set_xlabel("FPR", fontsize=16)
            ax.set_title("ROC curve of {}".format(self.name), fontsize=32)
        ax.grid(True)
        ax.legend(fontsize=12, loc=4)
        roc.savefig("{}ROC curve of {}.pdf".format(save_location, self.name))

        if not plot_results:
            plt.close()

        # Evaluation report
        model_metric_location = "../Data/model_metrics.csv"
        if not os.path.exists(model_metric_location):
            with open(model_metric_location, "w") as f:
                f.write("Model,Exercise,Main_Label,Precision,Recall,F1-score,Support,Date")

        classification_dict = classification_report(val_true, val_pred, target_names=target_names, output_dict=True)

        for count, key in enumerate(classification_dict.keys()):
            if count > 1:
                break
            report_line = "\n{},{},{},{},{},{},{}".format(self.name,
                                                          self.exercise,
                                                          key,
                                                          classification_dict[key]["precision"],
                                                          classification_dict[key]["recall"],
                                                          classification_dict[key]["f1-score"],
                                                          classification_dict[key]["support"],
                                                          time.asctime())
            with open(model_metric_location, "a") as f:
                f.write(report_line)
        if print_results:
            print(classification_report(val_true, val_pred, target_names=target_names))

        # showing plots if plot_results is True
        if plot_results:
            plt.show()
        return model.predict_proba(train[0])[:, 1], val_proba[:, 1]

    def predict(self, patient, selected_sensors=None, exercise=None, plot_results=False):
        """
        This function predict the class of the patient

        :param patient: string, a string with the id of the patient
        :param selected_sensors: array-like, the sensors that are going to be used for the prediction
        :param exercise: int, the exercise that is used for the prediction
        :param plot_results: boolean, if this boolean is set to true it will print the prediction plot
        :return:
        """
        if not self.has_run:
            print("[ERROR] Classifier has not been trained and evaluated, exiting function")
            return 0

        # Loading in the models and setting the target names
        model = self.load_model()

        # Checking if an exercise is given, if this is not given than it will use the exercise from the classifier objec
        if exercise is None:
            exercise = self.exercise

        if selected_sensors is None:
            selected_sensors = self.selected_sensors

        pred = pd.DataFrame()
        for count, clf in enumerate(self.classifiers):
            prediction = clf.predict(patient=patient, exercise=exercise, selected_sensors=selected_sensors)[2]
            prediction_df = pd.DataFrame(prediction, columns=["{}.{}".format(clf.label_a, count),
                                                              "{}.{}".format(clf.label_b, count)])
            if pred.empty:
                pred = prediction_df
            else:
                pred = pred.join(prediction_df)

        model_pred = model.predict(pred)
        model_proba = model.predict_proba(pred)
        total_pred = Counter(model_pred).most_common(1)[0][0]

        predict_plot = plt.figure(figsize=FIGSIZE)
        ax = predict_plot.add_subplot(111)

        for count, label in enumerate(self.labels):
            ax.plot(model_proba[:, count], label=label)
        ax.set_title("Prediction of patient N{} with exercise {}".format(patient, exercise))
        ax.set_xlabel("Windows")
        ax.set_ylabel("Probability")
        ax.legend(fontsize=16)

        # Showing the plot if plot_result is true
        if plot_results:
            plt.show()
        plt.close()
        return total_pred, model_pred, model_proba

    def predict_video(self, patient, exercise=None, selected_sensors=None):
        """
        This function is the same as OneVsAll.predict, but it will also make a video with the classification

        Known issues:
         - timing isn't perfect, this is due to a quirk with openCV's frame rate calculation causing the frame rate not
           to be a rounded 30 fps but 29,7 fps or 29,4 fps
         - progress bar is not always the same amount of #

        :param patient: String, the ID of the patient that you want to classify
        :param exercise: Int, the exercise that is used for the classification
        :param selected_sensors: array_like, the sensors that are used for the classification, sensors can be found in
                                             the sensorEMG.csv and sensorIMU_EMG.csv files
        :return: list, a list containing the classification of the patient, the classification per window and the
                       probabilities per window
        """
        # Loading in model and setting the target_names
        model = self.load_model()
        classification_label = self.labels

        if selected_sensors is None:
            selected_sensors = self.selected_sensors

        if exercise is None:
            exercise = self.exercise

        # Retrieving the data for the patient
        pred = pd.DataFrame()
        for count, clf in enumerate(self.classifiers):
            prediction = clf.predict(patient=patient, exercise=exercise, selected_sensors=selected_sensors)[2]
            prediction_df = pd.DataFrame(prediction, columns=["{}.{}".format(clf.label_a, count),
                                                              "{}.{}".format(clf.label_b, count)])
            if pred.empty:
                pred = prediction_df
            else:
                pred = pred.join(prediction_df)

        # Generating all the predictions
        predictions = model.predict_proba(pred)
        round_predictions = model.predict(pred)
        total_prediction = Counter(round_predictions).most_common(1)[0][0]

        # Checking if the save directories exists
        save_location = "{}/Classifications/".format(SAVE_LOCATION)
        if not os.path.exists(save_location):
            os.mkdir(save_location)

        save_patient = "{}/Nemo_0{}/".format(save_location, patient)
        if not os.path.exists(save_patient):
            os.mkdir(save_patient)

        video_location = "G:/NEMO/Recordings/Nemo_0{}/".format(patient)

        classification_colors = ["blue", "green", "red"]
        for item in glob.glob("{}*.avi".format(video_location)):
            basename = os.path.basename(item)
            if basename.startswith(str(exercise)):
                # Opening the input video and creating a output video
                cap = cv2.VideoCapture(item)

                fourcc = cv2.VideoWriter_fourcc(*"XVID")
                out = cv2.VideoWriter(
                    "{}N{}_{}_Classification_{}.avi".format(save_patient, patient, exercise, self.name),
                    fourcc,
                    30,
                    (750, 480))

                # Setting a few counters
                count = 0  # Enumerate functionality for the while loop
                line = 0  # Keeps the value of the vertical line on the y-axis
                sec = 0  # the amount of seconds that have passed in the video

                frame_rate = 30
                sec_amount = (int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) / frame_rate) / len(predictions)

                time = [round(x * sec_amount, 1) for x in range(len(predictions))]
                line_amount = max(time) / int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

                # Setting up the progress bar
                toolbar_width = 50

                sys.stdout.write("\t > Generating prediction video for patient N{} with model {} \n"
                                 "\t   %s".format(patient, self.name) % (" " * toolbar_width))
                sys.stdout.flush()
                sys.stdout.write("\b" * (toolbar_width + 1))

                # opening the video and reading it frame by frame
                while cap.isOpened():
                    ret, frame = cap.read()
                    if ret:

                        if count % round(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) / toolbar_width) == 0 and count != 0:
                            sys.stdout.write("#")
                            sys.stdout.flush()

                        # Setting the plots
                        fig = plt.figure(figsize=(7.5, 4.8))
                        ax1 = plt.axes([0, 0.3, 1, 0.7])
                        ax2 = plt.axes([0.1, 0.1, 0.8, 0.15])

                        # Adding a line that shows the prediction
                        ax2.axvline(line, c="black", linestyle="--")
                        line += line_amount

                        if round(sec, 1) in time:
                            index = time.index(round(sec, 1))

                            max_index = list(predictions[index]).index(max(predictions[index]))
                            classification = classification_label[max_index], classification_colors[max_index]

                        sec += 1 / cap.get(cv2.CAP_PROP_FPS)

                        # Changing the frame from BGR to RGB for matplotlib
                        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

                        # Plotting the frame and adding a the classification label
                        ax1.imshow(frame)
                        ax1.axis("off")
                        ax1.text(x=20, y=450,
                                 s=classification[0],
                                 fontsize=22,
                                 ha="left",
                                 color=classification[1])

                        # Plotting the predictions that where made for this patient
                        for c, label in enumerate(classification_label):
                            ax2.plot(time, predictions[:, c], label=label)
                        ax2.set_title("Probabilities for the different diagnoses",
                                      x=0,
                                      ha="left")
                        ax2.set_ylabel("Probability", fontsize=8)
                        ax2.set_xlim((min(time), max(time)))
                        ax2.set_xlabel("Time (Secs)", fontsize=8)
                        ax2.legend(loc=4, fontsize=8, bbox_to_anchor=(1.1, 1))

                        # Converting the figure to a string and changing it back to BGR for openCV
                        fig.canvas.draw()

                        data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep="")
                        data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))

                        data = cv2.cvtColor(data, cv2.COLOR_RGB2BGR)

                        # Saving the data to the video and removing the plots from the memory
                        out.write(data)
                        fig.clf()
                        plt.close(fig)
                        gc.collect()
                        count += 1
                        if cv2.waitKey(1) & 0xFF == ord("q"):
                            break
                    else:
                        break
                sys.stdout.write("\n")

        return total_prediction, round_predictions, predictions


class OneVsOne(OneVsAll):
    """
    This class is used as a OneVsOne classifier between two labels and can train and validate the classifier. It can also
    predict patients from the NEMO data set

    This class uses the create_model, load_model and run function from the OneVsAll class

    :param data: pandas DataFrame, This is the data that is used for the training and validation of the classifier
    :param exercise: int, the exercise that is used for the training of the classifier
    :param train: array-like or pandas DataFrame, the array-like contains all the patients in the train data set and the
                                                  the pandas DataFrame contains all the data for the train data set
    :param validation: array-like or pandas DataFrame, the array-like contains all the patients in the validation data
                                                       set and the pandas DataFrame contains all the data for the
                                                       validation set
    :param label_a: string, this is the first label given to the 0 classification
    :param label_b: string, this is the second label given to the 1 classification
    :param name: sting, this is the name of the classifier, this is also used for the creation of the save name of the
                        trained classifier
    :param model: skLearn classifier object, this is the model used for the creation of the classifier
    :param modality: string, this is the modality that is used for the classification
    :param low: int, the amount of Hz that is used for the low pass filter of the cross correlation
    :param selected_sensors: array-like, this array-like contains the sensors that are used for the training and
                                         validation of the classifier
    :param window_size: int, this is the window size that is used for the extraction of the features
    :param window_step: int, this is the size of the step that is used for the overlapping windows
    """
    def __init__(self, data, exercise, train, validation, label_a, label_b, name, model=None, modality="IMU_EMG",
                 low=[4, 30], selected_sensors=[4, 6], window_size=3000, window_step=333):
        self.data = data
        self.exercise = exercise
        self.train = train
        self.validation = validation
        self.label_a = label_a
        self.label_b = label_b
        self.name = name
        self.save_name = "../Data/Model/{}.joblib".format(name)
        self.model = model
        self.modality = modality
        self.low = low
        self.selected_sensors = selected_sensors
        self.window_size = window_size
        self.window_step = window_step
        self.has_run = False

    def generate_data(self):
        """
        This function retrieves the test and train set used for this classifier object, the selection is based on the
        self.train and self.validation parameters given for this object, if the self.train and self.validate are
        already the right format, the funtion will just pas this information on

        :return: array-like, this is an array-like containing to lists each list contains the pure data without labels
                             and a list containing all the labels for each window
        """
        if type(self.train[0]) == pd.core.frame.DataFrame or type(self.validation[0]) == pd.core.frame.DataFrame:
            train = self.train
            train[1] = [0 if x == self.label_a else 1 for x in train[1]]

            validation = self.validation
            validation[1] = [0 if x == self.label_a else 1 for x in validation[1]]

        else:
            # Retrieving the train data and train labels
            train_data = self.data.loc[self.data["patient"].isin(self.train)]
            train_label = train_data["diagnosis"]

            train_data = train_data.drop(["patient", "diagnosis"], axis=1)
            if "Unnamed: 0" in train_data:
                train_data = train_data.drop(["Unnamed: 0"], axis=1)

            train_label = [0 if x == self.label_a else 1 for x in train_label]

            train = [train_data, train_label]

            # Retrieving the validation data and the validation labels
            validation_data = self.data.loc[self.data["patient"].isin(self.validation)].drop(["patient", "diagnosis"],
                                                                                             axis=True)
            if "Unnamed: 0" in validation_data:
                validation_data = validation_data.drop(["Unnamed: 0"], axis=1)
            validation_label = self.data.loc[self.data["patient"].isin(self.validation)]["diagnosis"]
            validation_label = [0 if x == self.label_a else 1 for x in validation_label]
            validation = [validation_data, validation_label]
        return train, validation

    def evaluate_model(self, model, train, validation, print_results=False, plot_results=False):
        """
        This function evaluates the model and returns the evaluation plot and metrics

        :param model: trained skLearn Classifier object, this is the model that is used for the main classification
        :param train: array-like, an array-like with a dataframe with all the data and a list with the labels
        :param validation: array-like, an array-like with a dataframe with all the data and a list with the labels
        :param print_results: boolean, if this boolean is True, the function will print to the commandline
        :param plot_results: boolean, if this boolean is True, the function will show the plots at the end
                             of the function
        :return: the classifications of the train data and the validation data
        """
        # Creating a save location
        save_location = "{}/ClassifierPlots/".format(SAVE_LOCATION)
        if not os.path.exists(save_location):
            os.mkdir(save_location)

        if print_results:
            print("\t > Evaluating model {}".format(self.name))

        # Retrieving labels and predictions
        val_pred = model.predict(validation[0])
        val_proba = model.predict_proba(validation[0])
        val_true = validation[1]

        target_names = [self.label_a, self.label_b]

        # Generating a feature importance plot if a tree classifier is used
        try:
            feat_imp = pd.Series(model.feature_importances_, list(train[0])).sort_values(ascending=False)[:30]

            feat_plot = plt.figure(figsize=FIGSIZE)
            axes = feat_plot.add_subplot(111)

            feat_imp.plot(kind="barh", ax=axes)
            axes.set_title("Feature importance of {}".format(self.name),
                           ha="left",
                           x=0)
            feat_plot.savefig("{}Featplot_{}.pdf".format(save_location, self.name))

            if not plot_results:
                plt.close()

        except AttributeError:
            if print_results:
                print("\t [WARNING] Couldn't generate feature importance for this model, code will skip this part")

        # Confusion Matrix
        conf = confusion_matrix(val_true, val_pred)

        conf_matrix = plt.figure(figsize=FIGSIZE)
        ax = conf_matrix.add_subplot(111)

        sns.heatmap(conf,
                    ax=ax,
                    fmt="d",
                    annot=True,
                    yticklabels=target_names,
                    xticklabels=target_names
                    )
        ax.set_ylabel("True label")
        ax.set_xlabel("Predicted label")
        ax.set_title("Confusion matrix of {}".format(self.name))

        conf_matrix.savefig("{}Confusion matrix for {}.pdf".format(save_location, self.name))

        if not plot_results:
            plt.close()

        # ROC curve
        roc = plt.figure(figsize=FIGSIZE)
        fpr, tpr, _ = roc_curve(val_true, val_proba[:, 1])

        # Code to save all the data for a combined ROC curve
        fpr_to_csv = np.append(np.array([self.name, "fpr", roc_auc_score(val_true, val_proba[:, 1])]), fpr)
        tpr_to_csv = np.append(np.array([self.name, "tpr", roc_auc_score(val_true, val_proba[:, 1])]), tpr)

        with open("{}ROC.csv".format(save_location), "a") as f:
            writer = csv.writer(f)
            writer.writerow(fpr_to_csv)
            writer.writerow(tpr_to_csv)

        ax = roc.add_subplot(111)
        ax.plot(fpr, tpr, label="Area Under the Curve = {}".format(roc_auc_score(val_true, val_proba[:, 1])))
        ax.plot([0, 1], [0, 1], c="black", linestyle="--", linewidth=0.75)
        ax.set_ylabel("True Positive Rate (TRP)")
        ax.set_xlabel("False Positve Rate (FRP)")
        ax.set_title("ROC curve of {}".format(self.name))
        ax.grid(True)
        ax.legend(fontsize=8, loc=4)
        roc.savefig("{}ROC curve of {}.pdf".format(save_location, self.name))

        if not plot_results:
            plt.close()

        # Evaluation report
        model_metric_location = "../Data/model_metrics.csv"
        if not os.path.exists(model_metric_location):
            with open(model_metric_location, "w") as f:
                f.write("Model,Exercise,Main_Label,Precision,Recall,F1-score,Support,Date")

        classification_dict = classification_report(val_true, val_pred, target_names=target_names, output_dict=True)

        for count, key in enumerate(classification_dict.keys()):
            if count > 1:
                break
            report_line = "\n{},{},{},{},{},{},{}".format(self.name,
                                                          self.exercise,
                                                          key,
                                                          classification_dict[key]["precision"],
                                                          classification_dict[key]["recall"],
                                                          classification_dict[key]["f1-score"],
                                                          classification_dict[key]["support"],
                                                          time.asctime())
            with open(model_metric_location, "a") as f:
                f.write(report_line)
        if print_results:
            print(classification_report(val_true, val_pred, target_names=target_names))

        # showing plots if plot_results is True
        if plot_results:
            plt.show()
        return model.predict_proba(train[0])[:, 1], val_proba[:, 1]

    def predict(self, patient, selected_sensors=None, exercise=None):
        """
        This function predict the class of the patient

        :param patient: string, a string with the id of the patient
        :param selected_sensors: array-like, the sensors that are going to be used for the prediction
        :param exercise: int, the exercise that is used for the prediction
        :return: list, this list contains the classification of the patient, all the label predictions for
                       each window and all the probabilities for each window
        """
        # checking the sensors
        if selected_sensors is None:
            selected_sensors = self.selected_sensors

        # Loading in the models and setting the target names
        model = self.load_model()
        target_names = [self.label_a, self.label_b]

        # Checking if an exercise is given,
        # if this is not given than it will use the exercise from the classifier object
        if exercise is None:
            exercise = self.exercise

        # Retrieving the data for the patient
        emg_signal = EmgSignal(patient=patient,
                               exercise=exercise,
                               modality=self.modality,
                               selected_sensors=selected_sensors,
                               low=self.low,
                               window_size=self.window_size,
                               window_step=self.window_step)

        # Extracting the features and dropping the patient and diagnosis column if it is in the data
        patient_data = emg_signal.ml_output(lag=range(-100, 101, 5)).drop(["diagnosis", "patient"], axis=1)
        patient_data = normalize_features(patient_data, overwrite=False, exercise=exercise)

        predict_probas = model.predict_proba(patient_data)
        predictions = model.predict(patient_data)

        total_prediction = target_names[int(round(np.mean(predictions)))]
        return total_prediction, predictions, predict_probas

    def predict_video(self, patient, exercise=None, selected_sensors=None):
        """
        This function works the same as the predict function but it also prduces a video of the prediction combined with
        the task the patient is doing

        Known issues:
         - timing isn't perfect, this is due to a quirk with openCV's frame rate calculation causing the frame rate not
           to be a rounded 30 fps but 29,7 fps or 29,4 fps
         - progress bar is not always the same amount of #

        :param patient: String, the ID of the patient that you want to classify
        :param exercise: Int, the exercise that is used for the classification
        :param selected_sensors: array_like, the sensors that are used for the classification, sensors can be found in
                                             the sensorEMG.csv and sensorIMU_EMG.csv files
        :return: list, a list containing the classification of the patient, the classification per window and the
                       probabilities per window
        """
        # checking the sensors
        if selected_sensors is None:
            selected_sensors = self.selected_sensors

        # Loading in model and setting the target_names
        model = self.load_model()
        classification_label = [self.label_a, self.label_b]

        # Checking if an exercise is given, if no exercise is given it will use the exercise from the OneVsOne object
        if exercise is None:
            exercise = self.exercise

        # Retrieving the data for the patient
        emg_signal = EmgSignal(patient=patient,
                               exercise=exercise,
                               modality=self.modality,
                               selected_sensors=selected_sensors,
                               low=self.low,
                               window_step=self.window_step,
                               window_size=self.window_size)

        patient_data = emg_signal.ml_output(lag=range(-100, 101, 5)).drop(["diagnosis", "patient"], axis=1)
        patient_data = normalize_features(patient_data, overwrite=False, exercise=exercise)

        predictions = model.predict_proba(patient_data)

        save_location = "{}/Classifications/".format(SAVE_LOCATION)
        if not os.path.exists(save_location):
            os.mkdir(save_location)

        save_patient = "{}/Nemo_0{}/".format(save_location, patient)
        if not os.path.exists(save_patient):
            os.mkdir(save_patient)

        video_location = "G:/NEMO/Recordings/Nemo_0{}/".format(patient)

        classification_colors = ["blue", "green"]
        for item in glob.glob("{}*.avi".format(video_location)):
            basename = os.path.basename(item)
            if basename.startswith(str(exercise)):
                # Setting the output file
                fourcc = cv2.VideoWriter_fourcc(*"XVID")
                out = cv2.VideoWriter(
                    "{}N{}_{}_Classification_{}.avi".format(save_patient, patient, exercise, self.name),
                    fourcc,
                    30,
                    (750, 480))

                # Setting the input file
                cap = cv2.VideoCapture(item)
                classification = "None", "black"

                count = 0
                line = 0
                sec = 0
                frame_rate = 30
                sec_amount = (int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) / frame_rate) / len(predictions)

                time = [round(x * sec_amount, 1) for x in range(len(predictions))]
                line_amount = max(time) / int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

                # Setting up the progress bar
                toolbar_width = 50

                sys.stdout.write("\t > Generating prediction video for patient N{} with model {} \n"
                                 "\t   [%s]".format(patient, self.name) % (" " * toolbar_width))
                sys.stdout.flush()
                sys.stdout.write("\b" * (toolbar_width+1))

                # opening the video and reading it frame by frame
                while cap.isOpened():
                    ret, frame = cap.read()
                    if ret:

                        if count % round(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) / toolbar_width) == 0 and count != 0:
                            sys.stdout.write("#")
                            sys.stdout.flush()

                        fig = plt.figure(figsize=(7.5, 4.8))
                        ax1 = plt.axes([0, 0.3, 1, 0.7])
                        ax2 = plt.axes([0.1, 0.1, 0.8, 0.15])

                        # Adding a line that shows the prediction
                        ax2.axvline(line, c="black", linestyle="--")
                        line += line_amount

                        if round(sec, 1) in time:
                            index = time.index(round(sec, 1))

                            max_index = list(predictions[index]).index(max(predictions[index]))
                            classification = classification_label[max_index], classification_colors[max_index]

                        sec += 1 / cap.get(cv2.CAP_PROP_FPS)

                        # Changing the frame from BGR to RGB for matplotlib
                        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

                        # Plotting the frame and adding a the classification label
                        ax1.imshow(frame)
                        ax1.axis("off")
                        ax1.text(x=20, y=450,
                                 s=classification[0],
                                 fontsize=22,
                                 ha="left",
                                 color=classification[1])

                        # Plotting the predictions that where made for this patient
                        for c, label in enumerate(classification_label):
                            ax2.plot(time, predictions[:, c], label=label)
                        ax2.set_title("Probabilities for the different diagnoses",
                                      x=0,
                                      ha="left")
                        ax2.set_ylabel("Probability", fontsize=8)
                        ax2.set_xlabel("Time (Secs)", fontsize=8)
                        ax2.legend(loc=4, fontsize=8, bbox_to_anchor=(1.1, 1))

                        # Converting the figure to a string and changing it back to BGR for openCV
                        fig.canvas.draw()

                        data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep="")
                        data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))

                        data = cv2.cvtColor(data, cv2.COLOR_RGB2BGR)

                        # Saving the data to the video and removing the plots from the memory
                        out.write(data)
                        fig.clf()
                        plt.close(fig)
                        gc.collect()
                        count += 1
                        if cv2.waitKey(1) & 0xFF == ord("q"):
                            break
                    else:
                        break
                sys.stdout.write("]\n")

        round_predictions = model.predict(patient_data)
        total_prediction = classification_label[int(round(np.mean(round_predictions)))]
        return total_prediction, round_predictions, predictions

    def predict_window(self, window):
        """
        This function predict the label for one window

        :param window: array-like, the window that will be predicted
        :return: list, a list containing the classification of the window, the binary label of the window and the
                       probability of the window
        """
        model = self.load_model()
        classification_labels = [self.label_a, self.label_b]

        classification = model.predict(window)
        proba = model.predict_proba(window)
        label = classification_labels[classification]
        return label, classification_labels, proba

    def __str__(self):
        return self.name


def argument_parser():
    """
    This function retrieves the arguments from the commandline

    :return: tuple, a tuple with all the parameters from the commandline
    """
    parse = argparse.ArgumentParser(description="This script is used for the training of a classifier using the labels "
                                                "Healthy, Tremor, Myoclonus. This script can also be used for the "
                                                "prediction of patients.")
    parse.add_argument("-p",
                       "--patients",
                       help="These are the patients used for the training and validation of the classifier",
                       nargs="+",
                       required=True)
    parse.add_argument("-e",
                       "--exercises",
                       help="The exercise or exercises that are going to be used for the analysis",
                       required=True,
                       nargs="+")
    parse.add_argument("-m",
                       "--modality",
                       help="The modality that is going to be used. "
                            "Only 'IMU_EMG' and 'EMG' are supported at the moment",
                       required=False,
                       default="IMU_EMG")
    parse.add_argument("-s",
                       "--step",
                       help="The step that is made for the overlapping windows while extracting the EMG features",
                       required=False,
                       default=333,
                       type=int)
    parse.add_argument("-n",
                       "--size",
                       help="The size of the overlapping windows that are used to extract the EMG features",
                       required=False,
                       default=3000,
                       type=int)
    parse.add_argument("-f",
                       "--filter",
                       help="the amount of Hz that is used in the lowpass filter on the mean-corrected rectified "
                            "EMG signal that is used to extract the cross correlations",
                       required=False,
                       nargs="+",
                       default=[4, 20],
                       type=list)
    parse.add_argument("-r",
                       "--runs",
                       help="The amount of times it will create models",
                       required=False,
                       default=1,
                       type=int)

    # Somehow this didn't work correctly with bool, so this was set to string and later converted to Boolean
    parse.add_argument("-o",
                       "--overwrite",
                       help="If this boolean is set to true it will overwrite the existing data file",
                       required=False,
                       default="False",
                       type=str)
    parse.add_argument("-P",
                       "--predict",
                       help="The patients that you want a classification for",
                       required=False,
                       default=[],
                       nargs="+")
    parse.add_argument("-S",
                       "--sensors",
                       help="The sensors that are going to be used for classification and training",
                       required=False,
                       default=[4, 6],
                       nargs="+")

    args = parse.parse_args()

    selected_sensors = [int(x) for x in args.sensors]

    # Converting overwrite String to Boolean
    if args.overwrite in ["True", "true", "t", "T"]:
        overwrite = True
    else:
        overwrite = False

    exercises = [int(x) for x in args.exercises]
    return_tuple = (args.patients, exercises, args.modality, args.step, args.size, args.filter, args.runs, overwrite,
                    args.predict, selected_sensors)
    return return_tuple


def generate_one_vs_one(data, tags, labels, exercise, window_step, window_size, low, print_results=False,
                        overwrite=True, plot_results=False, selected_sensors=[4, 6]):
    """
    This function generates the different OneVsOne objects with each diagnosis combination

    :param data: Pandas DataFrame, this is the data that is used for the training and validation of the models
    :param tags: dict, these are the diagnosis tags with each of their patients
    :param labels: array-like, these are the diagnoses of wich you want to create OneVsOne objects
    :param exercise: int, the exercise of the data
    :param window_step: int, the sample step that is used for the window creation
    :param window_size: int, the size of the sample windows
    :param low: int, the amount of Hz of the low bandpass filter that is used for the cross correlation
    :param print_results: boolean, if this boolean is set to true it will print the results of the classifiers
    :param overwrite: boolean, if this boolean is set to true it will overwrite the existing models
    :param plot_results: boolean, if this boolean is set to true it will show the plots made from the classifiers
    :param selected_sensors: array-like, an array-like containing all the sensor that are used for the training and
                                         validation of the OneVsOne classifiers
    :return: a list with all the classifiers, a list with only the AB classifiers and a list with the BA classifiers
    """
    classifiers = []
    ab_classifiers = []
    ba_classifiers = []
    trad_classifiers = []

    # Extracting each first label
    for count, label_a in enumerate(labels):
        # Extracting each label after label_a, this prevents the code from making MyoclonusVsTremor and
        # TremorVsMyoclonus since these are practically the same
        for label_b in labels[count + 1:]:
            # Retrieving data for the right diagnoses
            diagnoses_data = data[data["diagnosis"].isin([label_a, label_b])]
            train_data, validate_data, train_labels, validate_labels = train_test_split(
                diagnoses_data.drop(["diagnosis", "patient"], axis=1),
                diagnoses_data["diagnosis"])

            # Generating 2 test and train sets one for AB training and one for BA training
            train_a = tags[label_a][:-1] + tags[label_b][:-1]
            validation_a = [tags[label_a][-1], tags[label_b][-1]]

            train_b = tags[label_a][1:] + tags[label_b][1:]
            validation_b = [tags[label_a][0], tags[label_b][0]]

            # Generating the AB classifier
            ab_classifier = OneVsOne(data=data,
                                     name="{}Vs{}_A_Exercise_{}".format(label_a, label_b, exercise),
                                     exercise=exercise,
                                     train=train_a,
                                     validation=validation_a,
                                     label_a=label_a,
                                     label_b=label_b,
                                     window_step=window_step,
                                     window_size=window_size,
                                     low=low,
                                     model=MLPClassifier(max_iter=25),
                                     selected_sensors=selected_sensors)

            # Training and validating the classifier
            ab_classifier.run(overwrite=overwrite, print_results=print_results, plot_results=plot_results)

            # Generating the BA classifier
            ba_classifier = OneVsOne(data=data,
                                     name="{}Vs{}_B_Exercise_{}".format(label_a, label_b, exercise),
                                     exercise=exercise,
                                     train=train_b,
                                     validation=validation_b,
                                     label_a=label_a,
                                     label_b=label_b,
                                     window_step=window_step,
                                     window_size=window_size,
                                     low=low,
                                     model=MLPClassifier(max_iter=25),
                                     selected_sensors=selected_sensors)

            # Training and validating the classifier
            ba_classifier.run(overwrite=overwrite, print_results=print_results, plot_results=plot_results)

            trad_classifier = OneVsOne(data=data,
                                       name="{}Vs{}_Trad_Exercise_{}".format(label_a, label_b, exercise),
                                       exercise=exercise,
                                       train=[train_data, train_labels],
                                       validation=[validate_data, validate_labels],
                                       label_a=label_a,
                                       label_b=label_b,
                                       window_step=window_step,
                                       window_size=window_size,
                                       low=low,
                                       model=MLPClassifier(max_iter=25),
                                       selected_sensors=selected_sensors)

            trad_classifier.run(overwrite=overwrite, print_results=print_results, plot_results=plot_results)

            # Saving the classifiers in a list
            ab_classifiers.append(ab_classifier)
            ba_classifiers.append(ba_classifier)
            trad_classifiers.append(trad_classifier)

            classifiers.append(ab_classifier)
            classifiers.append(ba_classifier)
    return classifiers, ab_classifiers, ba_classifiers, trad_classifiers


def create_meta_train_validation(classifiers):
    """
    This function creates the validation and train tags for the meta classifier by combining the train and validation
    tags from the other classifiers

    :param classifiers: array-like, an array-like with all the classifier objects
    :return: a list with all the train patients and a list with all the validation patients
    """
    train = []
    validation = []

    # Combining all the train lists and validation list fromm all the classifier objects
    for clf in classifiers:
        train += clf.train
        validation += clf.validation

    # Only saving the unique values
    train = list(np.unique(np.array(train)))
    validation = list(np.unique(np.array(validation)))
    return train, validation


def generate_tags(patients):
    """
    This function generates the tags for the machine learning algorithm

    :param patients: array-like, a list with all the patients
    :return: dictionary, a dictionary with all the patients sorted on diagnosis
    """
    tags = {}
    for patient in patients:
        diagnosis = DIAGNOSIS[patient]

        # Trying to append to the diagnosis label, if this wont work the tag doesn't exist
        try:
            tags[diagnosis].append("N" + patient)
        except KeyError:
            tags[diagnosis] = ["N" + patient]
    return tags


def main():
    # Removing the result file if it already exists
    if os.path.exists("../Data/model_metrics.csv"):
        os.remove("../Data/model_metrics.csv")

    # Parsing the commandline
    patients, exercises, modality, window_step, window_size, low, n_runs, overwrite, predict, selected_sensors =\
        argument_parser()

    # Generating the tags for the different diagnoses
    tags = generate_tags(patients)

    # Turning of the warnings because there is an error with numpy.shape in this version
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        for exercise in exercises:

            # Loading in the Data for the exercise
            file_name = "../Data/Exercise{}_Data.csv".format(exercise)
            data = retrieve_data(filename=file_name, patients=patients, exercise=exercise, modality=modality,
                                 overwrite=overwrite, selected_sensors=selected_sensors,
                                 window_step=window_step, window_size=window_size)

            # Running the code N amount of times to generate metrics
            for _ in range(n_runs):

                # Loading in all the OneVsOne Classifiers
                _, ab_classifiers, ba_classifiers, _ = generate_one_vs_one(data=data,
                                                                           tags=tags,
                                                                           labels=["Healthy", "Tremor",
                                                                                   "Myoclonus"],
                                                                           exercise=exercise,
                                                                           window_step=window_step,
                                                                           window_size=window_size,
                                                                           print_results=True,
                                                                           low=[4, 30],
                                                                           selected_sensors=selected_sensors)

                # Splitting train and validation
                train_ab, validation_ab = create_meta_train_validation(classifiers=ab_classifiers)
                train_ba, validation_ba = create_meta_train_validation(classifiers=ba_classifiers)

                # Generating the AB Classifier
                meta_one_vs_one_a = MetaOneVsOne(data=data,
                                                 name="MetaOneVsOne_A_Exercise_{}".format(exercise),
                                                 train=train_ab,
                                                 validation=validation_ab,
                                                 labels=["Healthy", "Myoclonus", "Tremor"],
                                                 classifiers=ab_classifiers,
                                                 window_size=window_size,
                                                 window_step=window_step,
                                                 model=MLPClassifier(max_iter=25),
                                                 low=low,
                                                 exercise=exercise,
                                                 selected_sensors=selected_sensors)

                # Generating the BA Classifier
                meta_one_vs_one_b = MetaOneVsOne(data=data,
                                                 name="MetaOneVsOne_B_Exercise_{}".format(exercise),
                                                 train=train_ba,
                                                 validation=validation_ba,
                                                 labels=["Healthy", "Myoclonus", "Tremor"],
                                                 classifiers=ba_classifiers,
                                                 window_size=window_size,
                                                 window_step=window_step,
                                                 model=MLPClassifier(max_iter=25),
                                                 low=low,
                                                 exercise=exercise,
                                                 selected_sensors=selected_sensors)

                # Running both the meta classifiers
                # Training with the AB dataset
                meta_one_vs_one_a.run(overwrite=True, print_results=True, plot_results=False)

                # Training with the BA dataset
                meta_one_vs_one_b.run(overwrite=True, print_results=True, plot_results=False)

                # Preforming the predictions
                for patient in predict:
                    print(" > Classifing patient N{}".format(patient))

                    predict_a = meta_one_vs_one_a.predict_video(patient)
                    predict_b = meta_one_vs_one_b.predict_video(patient)

                    print(" > Prediction of patient N{} is: {} ({}), {} ({})".format(patient,
                                                                                     predict_a[0],
                                                                                     meta_one_vs_one_a.name,
                                                                                     predict_b[0],
                                                                                     meta_one_vs_one_b.name))

                    predict_test_a = meta_one_vs_one_a.predict_video(patient, exercise=23, selected_sensors=[5, 7])
                    predict_test_b = meta_one_vs_one_b.predict_video(patient, exercise=23, selected_sensors=[5, 7])

                    print(" > Predict with the other side: {} ({}), {} ({})".format(patient,
                                                                                    predict_test_a[0],
                                                                                    meta_one_vs_one_a.name,
                                                                                    predict_test_b[0],
                                                                                    meta_one_vs_one_b.name))
                return 0


if __name__ == '__main__':
    start = time.time()
    print(" > Started code one " + time.asctime())
    main()
    print(" > Code finished, took {} seconds".format(time.time() - start))
