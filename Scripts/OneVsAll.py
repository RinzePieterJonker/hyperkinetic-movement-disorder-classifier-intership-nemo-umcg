#!/usr/bin/env python3
"""
This script can be used as a classifier for the NEMO data, it also trains and validates the OneVsAll classifiers.

Usage:
OneVsAll.py --patients <PATIENTS ...> --exercise <EXERCISE ...>

Parameters:
 -p, --patients: the patients that are used for the training and classification of the classifier
 -e, --exercise: the exercise or exercise that is used for the creation of the object, if more than one exercise is
                 given it will create multiple classifiers

Optional Parameters:
 -h, --help     : this parameter shows the help of the arguments parser and will show the usage
 -m, --modality : this is the modality that used for the training and validation of the classifier and is also used for
                  the prediction of patients. Only 'IMU_EMG' and 'EMG' are supported and the default is 'IMU_EMG'


"""

# IMPORTS
import argparse
import cv2
import time
import warnings
import yaml
import csv
import os
import glob
import pickle
import gc
import sys

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from matplotlib import style
from Scripts.EmgSignal import EmgSignal, retrieve_data, normalize_features
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report, roc_auc_score, roc_curve, confusion_matrix


# METADATA
__author__ = "Rinze-Pieter Jonker"
__version__ = 0.5
__status__ = "W.I.P."

# GLOBALS
style.use("seaborn")
# Retrieving the globals form the config file
with open("../config.yaml", "r") as config:
    cfg = yaml.load(config)
    FIGSIZE = cfg["figsize"]
    SAVE_LOCATION = cfg["plot_save_location"]

    with open(cfg["diagnosis"]) as csv_file:
        csv_reader = csv.reader(csv_file)
        csv_lines = [x for x in csv_reader][1:]
        DIAGNOSIS = {}
        for line in csv_lines:
            DIAGNOSIS[line[0]] = line[1].strip(" ")


MODELS = {"knn": KNeighborsClassifier(n_neighbors=1),
          "naive_bayes": GaussianNB(),
          "logit": LogisticRegression(solver="lbfgs", multi_class="auto"),
          "svm": SVC(kernel="rbf", gamma="auto"),
          "decision_tree": DecisionTreeClassifier(),
          "random_forest": RandomForestClassifier(n_estimators=500),
          "mlp": MLPClassifier(),
          "GradBoost": GradientBoostingClassifier(n_estimators=20)
          }


# CODE
class OneVsAll:
    """
    This class functions as a classifier between one label and everything but that label.

    :param data: pandas DataFrame, the data used to train and validate the classifier
    :param name: string, the name of the classifier
    :param exercise: int, the exercise ID used for the training and validating of the classifier
    :param train_ids: array-like, this array-like contains all the patients that are in the train data set
    :param validation_ids: array-like, this array-like contains all the patients that are in the validation dat set
    :param modality: string, the modality that is used for this classifier
    :param main_label: string, the label that you want to classify
    :param model: skLearn classifier object, this is the model that is used
    :param low: int, the amount of Hz that is used in the low bandpass filter before the cross correlation calculation
    :param selected_sensors: array-like, this array-like contains all the sensor that are used for the training and
                                         validation of the classifier
    :param window_size: int, the size of the window that is used during feature extraction
    :param window_step: int, the size of the step that is used for the overlapping windows
    """
    def __init__(self, data, name, exercise, train_ids=None, validation_ids=None, modality="IMU_EMG",
                 main_label="Healthy", model=None, low=12, selected_sensors=[2, 4, 6], window_size=999,
                 window_step=333):
        self.data = data
        self.name = name
        self.exercise = exercise
        self.train = train_ids
        self.validation = validation_ids
        self.modality = modality
        self.main_label = main_label
        self.save_name = "../Data/Model/{}.joblib".format(name)
        self.model = model
        self.selected_sensors = selected_sensors
        self.low = low
        self.window_size = window_size
        self.window_step = window_step
        self.has_run = False

    def run(self, overwrite=True, print_results=True, plot_results=True):
        """
        This function trains and validates the classifiers

        :param overwrite: boolean, if this boolean is set to true it will overwrite existing classifiers
        :param print_results: boolean, if this boolean is set to true it will print the classification report to the
                                       command line
        :param plot_results: boolean, if this boolean is set to true it will show the generated plots instead of just
                                      saving them
        :return: a tuple containing the training labels and the test labels generated by the classifier
        """
        # Setting has run to True
        self.has_run = True

        # Retrieving the data
        train, validation = self.generate_data()

        # Checking if a classifier exists and making a new one if overwrite is true or if there is no classifier
        if not os.path.exists(self.save_name) or overwrite:
            model = self.create_model(train)
        else:
            model = self.load_model()

        # evaluating the model and
        train_labels, validation_labels = self.evaluate_model(model, train, validation,
                                                              print_results=print_results,
                                                              plot_results=plot_results)
        return train_labels, validation_labels

    def generate_data(self):
        """
        This function generates the train and validation data set, these are based of the self.train and self.validation
        lists

        :return: a tuple containing the train data set with its labels and the validation data set with its labels
        """
        if type(self.train) == list and type(self.train) == list:
            # Retrieving the train data and train labels
            train_data = self.data.loc[self.data["patient"].isin(self.train)]

            # Stratifing the training data
            main_data = train_data[train_data["diagnosis"] == self.main_label]
            not_main_data = train_data[train_data["diagnosis"] != self.main_label]

            # Calculating the downsample factor
            factor = round(not_main_data.shape[0] / main_data.shape[0])
            not_main_data = not_main_data[not_main_data.index % factor == 0]

            train_data = pd.concat([not_main_data, main_data], ignore_index=True)
            train_label = train_data["diagnosis"]

            train_data = train_data.drop(["patient", "diagnosis"], axis=1)
            if "Unnamed: 0" in train_data:
                train_data = train_data.drop(["Unnamed: 0"], axis=1)

            train_label = [1 if x == self.main_label else 0 for x in train_label]
            train = [train_data, train_label]

            # Retrieving the validation data and the validation labels
            validation_data = self.data.loc[self.data["patient"].isin(self.validation)].drop(["patient", "diagnosis"],
                                                                                             axis=True)
            if "Unnamed: 0" in validation_data:
                validation_data = validation_data.drop(["Unnamed: 0"], axis=1)
            validation_label = self.data.loc[self.data["patient"].isin(self.validation)]["diagnosis"]
            validation_label = [1 if x == self.main_label else 0 for x in validation_label]
            validation = [validation_data, validation_label]
        else:
            train_data = self.train.drop(["patient", "diagnosis"], axis=1)
            train_labels = self.train["diagnosis"]
            train_labels = [1 if x == self.main_label else 0 for x in train_labels]
            train = [train_data, train_labels]

            validation_data = self.validation.drop(["patient", "diagnosis"], axis=1)
            validation_labels = self.validation["diagnosis"]
            validation_labels = [1 if x == self.main_label else 0 for x in validation_labels]
            validation = [validation_data, validation_labels]
        return train, validation

    def create_model(self, train):
        """
        This function trains and saves the model retrieved from self.model, if model is None it will use a Multi Layer
        Percepteron with an input layer of 100 nodes

        :param train: array-like, an array-like containing the training data set and the training labels
        :return: a trained skLearn classifier object
        """
        # Checking if a model is given in the arguments
        if self.model is None:
            # Giving a default classifier
            self.model = MLPClassifier(max_iter=5)
            # self.model = RandomForestClassifier(n_estimators=1500)
        model = self.model.fit(train[0], train[1])

        # Saving the model
        with open(self.save_name, "wb") as f:
            pickle.dump(model, f)
        return model

    def load_model(self):
        """
        This function will load in the model from the Data/Model/ folder

        :return: trained skLearn Classifier object
        """
        with open(self.save_name, "rb") as f:
            model = pickle.load(f)
        return model

    def evaluate_model(self, model, train, validation, print_results=False, plot_results=False):
        """
        This function generates the statistics and plots on which the evaluation can be based on

        :param model: a trained skLearn classifier object, this is the classifier that needs to be evaluated
        :param train: array-like, an array-like containing the train data set and the train labels
        :param validation: array-like, an array-like containing the validation data set and the validation labels
        :param print_results: boolean, if this boolean is set to true it will print the results of the classification
                                       report to the commandline
        :param plot_results: boolean, if this boolean is set to true it will show the plots instead of just saving them
        :return:
        """
        # Creating a save location
        save_location = "{}/ClassifierPlots/".format(SAVE_LOCATION)
        if not os.path.exists(save_location):
            os.mkdir(save_location)

        if print_results:
            print("\t > Evaluating model {}".format(self.name))

        # Retrieving labels and predictions
        val_pred = model.predict(validation[0])
        val_proba = model.predict_proba(validation[0])
        val_true = validation[1]

        target_names = ["Not {}".format(self.main_label), self.main_label]

        # Generating a feature importance plot if a tree classifier is used
        try:
            feat_imp = pd.Series(model.feature_importances_, list(train[0])).sort_values(ascending=False)[:30]

            feat_plot = plt.figure(figsize=FIGSIZE)
            axes = feat_plot.add_subplot(111)

            feat_imp.plot(kind="barh", ax=axes)
            axes.set_title("Feature importance of {}".format(self.name),
                           ha="left",
                           x=0)
            feat_plot.savefig("{}Featplot_{}.pdf".format(save_location, self.name))

            if not plot_results:
                plt.close()

        except AttributeError:
            if print_results:
                print("\t [ERROR] Couldn't generate feature importance for this model, code will skip this part")

        # Confusion Matrix
        conf = confusion_matrix(val_true, val_pred)

        conf_matrix = plt.figure(figsize=FIGSIZE)
        ax = conf_matrix.add_subplot(111)

        sns.heatmap(conf,
                    ax=ax,
                    fmt="d",
                    annot=True,
                    yticklabels=target_names,
                    xticklabels=target_names
                    )
        ax.set_ylabel("True label")
        ax.set_xlabel("Predicted label")
        ax.set_title("Confusion matrix of {}".format(self.name))

        conf_matrix.savefig("{}Confusion matrix for {}.pdf".format(save_location, self.name))

        if not plot_results:
            plt.close()

        # ROC curve
        roc = plt.figure(figsize=FIGSIZE)
        fpr, tpr, _ = roc_curve(val_true, val_proba[:, 1])

        ax = roc.add_subplot(111)
        ax.plot(fpr, tpr, label="Area Under the Curve = {}".format(roc_auc_score(val_true, val_proba[:, 1])))
        ax.plot([0, 1], [0, 1], c="black", linestyle="--")
        ax.set_ylabel("True Positive Rate (TRP)")
        ax.set_xlabel("False Positve Rate (FRP)")
        ax.set_title("ROC curve of {}".format(self.name))
        ax.grid(True)
        ax.legend(fontsize=8, loc=4)
        roc.savefig("{}ROC curve of {}.pdf".format(save_location, self.name))

        if not plot_results:
            plt.close()

        # Evaluation report
        model_metric_location = "../Data/model_metrics.csv"
        if not os.path.exists(model_metric_location):
            with open(model_metric_location, "w") as f:
                f.write("Model,Exercise,Main_Label,Precision,Recall,F1-score,Support,Date")

        classification_dict = classification_report(val_true, val_pred, target_names=target_names, output_dict=True)

        for count, key in enumerate(classification_dict.keys()):
            if count > 1:
                break
            report_line = "\n{},{},{},{},{},{},{}".format(self.name,
                                                          self.exercise,
                                                          key,
                                                          classification_dict[key]["precision"],
                                                          classification_dict[key]["recall"],
                                                          classification_dict[key]["f1-score"],
                                                          classification_dict[key]["support"],
                                                          time.asctime())
            with open(model_metric_location, "a") as f:
                f.write(report_line)
        if print_results:
            print(classification_report(val_true, val_pred, target_names=target_names))

        # showing plots if plot_results is True
        if plot_results:
            plt.show()
        return model.predict_proba(train[0])[:, 1], val_proba[:, 1]

    def predict(self, patient, selected_sensors=[2, 4, 6], exercise=None):
        """
        This function is used to predict the diagnosis of a patient

        :param patient: string, the ID of the patient that you want to classify
        :param selected_sensors: array-like, an array-like containing all the sensors that are used during the
                                             classification
        :param exercise: int, the ID of the exercise that is used during the classification
        :return:
        """
        # TODO Finish this functions
        # Loading in the models and setting the target names
        model = self.load_model()

        # Checking if an exercise is given, if this is not given than it will use the exercise from the classifier objec
        if exercise is None:
            exercise = self.exercise

        # Retrieving the data for the patient
        emg_signal = EmgSignal(patient=patient,
                               exercise=exercise,
                               modality=self.modality,
                               selected_sensors=selected_sensors,
                               low=self.low,
                               window_size=self.window_size,
                               window_step=self.window_step)

        patient_data = emg_signal.ml_output().drop(["diagnosis", "patient"], axis=1)
        patient_data = normalize_features(patient_data, overwrite=False, exercise=exercise)

        # Calculating probabilities
        patient_class = model.predict(patient_data)
        patient_proba = model.predict_proba(patient_data)

        patient_total_class = sum(patient_class)
        patient_total_proba = list(sum(patient_proba))

        # patient_end_class = patient_total_class.index(max(patient_total_class))
        patient_end_proba = patient_total_proba.index(max(patient_total_proba))
        return patient_class, patient_proba, patient_end_proba

    def predict_video(self, patient, exercise=None, selected_sensors=[2, 4, 6]):
        # Loading in model and setting the target_names
        model = self.load_model()

        if exercise is None:
            exercise = self.exercise

        # Retrieving the data for the patient
        emg_signal = EmgSignal(patient=patient,
                               exercise=exercise,
                               modality=self.modality,
                               selected_sensors=selected_sensors,
                               low=self.low,
                               window_step=self.window_step,
                               window_size=self.window_size)

        patient_data = emg_signal.ml_output().drop(["diagnosis", "patient"], axis=1)
        patient_data = normalize_features(patient_data, overwrite=False, exercise=exercise)

        predictions = model.predict_proba(patient_data)

        save_location = "{}/Classifications/".format(SAVE_LOCATION)
        if not os.path.exists(save_location):
            os.mkdir(save_location)

        save_patient = "{}/Nemo_0{}/".format(save_location, patient)
        if not os.path.exists(save_patient):
            os.mkdir(save_patient)

        video_location = "G:/NEMO/Recordings/Nemo_0{}/".format(patient)

        classification_label = ["Not", self.main_label]
        classification_colors = ["blue", "green"]
        for item in glob.glob("{}*.avi".format(video_location)):
            basename = os.path.basename(item)
            if basename.startswith(str(exercise)):
                fourcc = cv2.VideoWriter_fourcc(*"XVID")
                out = cv2.VideoWriter(
                    "{}N{}_{}_Classification_{}.avi".format(save_patient, patient, exercise, self.name),
                    fourcc,
                    30,
                    (750, 480))

                cap = cv2.VideoCapture(item)
                classification = "None", "black"

                count = 0
                line = 0
                sec = 0
                sec_amount = (int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) / int(cap.get(cv2.CAP_PROP_FPS))) / len(
                    predictions)

                time = [round(x * sec_amount, 1) for x in range(len(predictions))]
                line_amount = max(time) / int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

                toolbar_width = 50

                sys.stdout.write("\t > Generating prediction video for patient N{} with model {} \n"
                                 "\t   [%s]".format(patient, self.name) % (" " * toolbar_width))
                sys.stdout.flush()
                sys.stdout.write("\b" * (toolbar_width+1))

                while cap.isOpened():
                    ret, frame = cap.read()
                    if ret:

                        if count % round(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) / toolbar_width) == 0 and count != 0:
                            sys.stdout.write("#")
                            sys.stdout.flush()

                        fig = plt.figure(figsize=(7.5, 4.8))
                        ax1 = plt.axes([0, 0.3, 1, 0.7])
                        ax2 = plt.axes([0.1, 0.1, 0.8, 0.15])

                        # Adding a line that shows the prediction
                        ax2.axvline(line, c="black", linestyle="--")
                        line += line_amount

                        if round(sec, 1) in time:
                            index = time.index(round(sec, 1))

                            max_index = list(predictions[index]).index(max(predictions[index]))
                            classification = classification_label[max_index], classification_colors[max_index]

                        sec += 1 / cap.get(cv2.CAP_PROP_FPS)

                        # Changing the frame from BGR to RGB for matplotlib
                        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

                        # Plotting the frame and adding a the classification label
                        ax1.imshow(frame)
                        ax1.axis("off")
                        ax1.text(x=20, y=450,
                                 s=classification[0],
                                 fontsize=22,
                                 ha="left",
                                 color=classification[1])

                        # Plotting the predictions that where made for this patient
                        for c, label in enumerate(classification_label):
                            ax2.plot(time, predictions[:, c], label=label)
                        ax2.set_title("Probabilities for the different diagnoses",
                                      x=0,
                                      ha="left")
                        ax2.set_ylabel("Probability", fontsize=8)
                        ax2.set_xlabel("Time (Secs)", fontsize=8)
                        ax2.legend(loc=4, fontsize=8, bbox_to_anchor=(1.1, 1))

                        # Converting the figure to a string and changing it back to BGR for openCV
                        fig.canvas.draw()

                        data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep="")
                        data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))

                        data = cv2.cvtColor(data, cv2.COLOR_RGB2BGR)

                        # Saving the data to the video and removing the plots from the memory
                        out.write(data)
                        fig.clf()
                        plt.close(fig)
                        gc.collect()
                        count += 1
                        if cv2.waitKey(1) & 0xFF == ord("q"):
                            break
                    else:
                        break
                sys.stdout.write("]\n")

def argument_parser():
    """
    This function retrieves the arguments from the commandline
    :return:
    """
    parse = argparse.ArgumentParser(description="Add description")
    parse.add_argument("-p",
                       "--patients",
                       help="Add help",
                       nargs="+",
                       required=True)
    parse.add_argument("-e",
                       "--exercises",
                       help="The exercise or exercises that are going to be used for the analysis",
                       required=True,
                       nargs="+")
    parse.add_argument("-m",
                       "--modality",
                       help="The modality that is going to be used. "
                            "Only 'IMU_EMG' and 'EMG' are supported at the moment",
                       required=False,
                       default="IMU_EMG")
    args = parse.parse_args()
    exercises = [int(x) for x in args.exercises]
    return args.patients, exercises, args.modality


def main():
    # Setting parameters
    window_size = 999
    window_step = 333

    low = 20

    train_ab = ["N09", "N24", "N30", "N17", "N21"]
    validation_ab = ["N10", "N14", "N26", "N15"]

    train_ba = ["N10", "N26", "N14", "N30", "N15"]
    validation_ba = ["N09", "N17", "N24", "N21"]

    overwrite = False
    n_runs = 1
    patients, exercises, modality = argument_parser()

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        for _ in range(n_runs):
            for exercise in exercises:
                file_name = "../Data/Exercise{}_Data.csv".format(exercise)
                data = retrieve_data(file_name, patients, exercise, modality, overwrite, selected_sensors=[4, 6],
                                     low=low, window_step=window_step, window_size=window_size)
                train, test = train_test_split(data)

                classifiers = []
                healthy_vs_all_a = OneVsAll(data=data,
                                            name="HealthyVsAll_A_exercise_{}".format(exercise),
                                            exercise=exercise,
                                            train_ids=train_ab,
                                            validation_ids=validation_ab,
                                            main_label="Healthy",
                                            low=low,
                                            window_size=window_size,
                                            window_step=window_step)
                classifiers.append(healthy_vs_all_a)

                healthy_vs_all_b = OneVsAll(data=data,
                                            name="HealthyVsAll_B_exercise_{}".format(exercise),
                                            exercise=exercise,
                                            train_ids=train_ba,
                                            validation_ids=validation_ba,
                                            main_label="Healthy",
                                            low=low,
                                            window_size=window_size,
                                            window_step=window_step)
                classifiers.append(healthy_vs_all_b)

                healthy_vs_all_c = OneVsAll(data=data,
                                            name="HealthyVsAll_auto_exercise_{}".format(exercise),
                                            exercise=exercise,
                                            train_ids=train,
                                            validation_ids=test,
                                            main_label="Healthy",
                                            low=low,
                                            window_size=window_size,
                                            window_step=window_step)
                # classifiers.append(healthy_vs_all_c)

                tremor_vs_all_a = OneVsAll(data=data,
                                           name="TremorVsAll_A_exercise_{}".format(exercise),
                                           exercise=exercise,
                                           train_ids=train_ab,
                                           validation_ids=validation_ab,
                                           main_label="Tremor",
                                           low=low,
                                           window_size=window_size,
                                           window_step=window_step)
                classifiers.append(tremor_vs_all_a)

                tremor_vs_all_b = OneVsAll(data=data,
                                           name="TremorVsAll_B_exercise_{}".format(exercise),
                                           exercise=exercise,
                                           train_ids=train_ba,
                                           validation_ids=validation_ba,
                                           main_label="Tremor",
                                           low=low,
                                           window_size=window_size,
                                           window_step=window_step)
                classifiers.append(tremor_vs_all_b)

                tremor_vs_all_c = OneVsAll(data=data,
                                           name="TremorVsAll_auto_exercise_{}".format(exercise),
                                           exercise=exercise,
                                           train_ids=train,
                                           validation_ids=test,
                                           main_label="Tremor",
                                           low=low,
                                           window_size=window_size,
                                           window_step=window_step)
                # classifiers.append(tremor_vs_all_c)

                myoclonus_vs_all_a = OneVsAll(data=data,
                                              name="MyoclonusVsAll_A_exercise_{}".format(exercise),
                                              exercise=exercise,
                                              train_ids=train_ab,
                                              validation_ids=validation_ab,
                                              main_label="Myoclonus",
                                              low=low,
                                              window_size=window_size,
                                              window_step=window_step)
                classifiers.append(myoclonus_vs_all_a)

                myoclonus_vs_all_b = OneVsAll(data=data,
                                              name="MyoclonusVsAll_B_exercise_{}".format(exercise),
                                              exercise=exercise,
                                              train_ids=train_ba,
                                              validation_ids=validation_ba,
                                              main_label="Myoclonus",
                                              low=low,
                                              window_size=window_size,
                                              window_step=window_step)
                classifiers.append(myoclonus_vs_all_b)

                myoclonus_vs_all_c = OneVsAll(data=data,
                                              name="MyoclonusVsAll_auto_exercise_{}".format(exercise),
                                              exercise=exercise,
                                              train_ids=train,
                                              validation_ids=test,
                                              main_label="Myoclonus",
                                              low=low,
                                              window_size=window_size,
                                              window_step=window_step)
                # classifiers.append(myoclonus_vs_all_c)

                dystonia_vs_all_a = OneVsAll(data=data,
                                             name="DystoniaVsAll_A_exercise_{}".format(exercise),
                                             exercise=exercise,
                                             train_ids=train_ab,
                                             validation_ids=validation_ab,
                                             main_label="Dystonia",
                                             low=low,
                                             window_size=window_size,
                                             window_step=window_step)
                classifiers.append(dystonia_vs_all_a)

                dystonia_vs_all_b = OneVsAll(data=data,
                                             name="DystoniaVsAll_B_exercise_{}".format(exercise),
                                             exercise=exercise,
                                             train_ids=train_ba,
                                             validation_ids=validation_ba,
                                             main_label="Dystonia",
                                             low=low,
                                             window_size=window_size,
                                             window_step=window_step)
                classifiers.append(dystonia_vs_all_b)

                for classifier in classifiers:
                    classifier.run(overwrite=True, plot_results=False, print_results=True)
    return 0


if __name__ == "__main__":
    start = time.time()
    print(" > Started code on {}".format(time.asctime()))
    main()
    print(" > Code finished, time elapsed: {}".format(time.time() - start))
